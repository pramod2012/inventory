<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    public $timestamps = false;
    protected  $table = 'tbl_stock';
    protected $primaryKey = 'stock_id';
    protected $fillable = ['category_id', 'brand_id', 'product_id', 'stock_quantity', 'stock_rate',
        'stock_paid_amount', 'stock_due_amount', 'stock_date', 'due_total', 'supplier_id', 'added_by', 'quantity_remains'
        ];
}
