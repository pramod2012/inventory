<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    public $timestamps = false;
    protected  $table = 'tbl_staff';
    protected $primaryKey = 'staff_id';
    protected $fillable = ['staff_name', 'staff_phone', 'staff_email', 'staff_password', 'category_perm', 'brand_perm', 'product_perm',
        'stock_perm', 'sell_perm', 'staff_address', 'entry_time'];
}
