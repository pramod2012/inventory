<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false;
    protected  $table = 'tbl_category';
    protected $primaryKey = 'category_id';
    protected $fillable = ['category_name', 'entry_time'];
}
