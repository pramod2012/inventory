<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Title extends Model
{
    public $timestamps = false;
    protected $table = 'tbl_title';
    protected $primaryKey = 'title_id';
    protected $fillable = ['title'];
}
