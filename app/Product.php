<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $timestamps = false;
    protected  $table = 'tbl_product';
    protected $primaryKey = 'product_id';
    protected $fillable = ['product_name', 'product_image', 'product_description', 'entry_time'];
}
