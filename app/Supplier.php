<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    public $timestamps = false;
    protected  $table = 'tbl_supplier';
    protected $primaryKey = 'supplier_id';
    protected $fillable = ['supplier_name', 'supplier_description', 'supplier_phone', 'supplier_email', 
        'supplier_website', 'supplier_address', 'entry_time'];
}
