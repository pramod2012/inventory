<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sell extends Model
{
    public $timestamps = false;
    protected $table = 'tbl_sell';
    protected $primaryKey = 'sell_id';
    protected $fillable = ['category_id', 'brand_id', 'product_id', 'sell_quantity', 'sell_rate', 
        'sell_total_price', 'sell_received_price', 'sell_due_price', 'sell_date', 'buyer_id', 'added_by'];
}
