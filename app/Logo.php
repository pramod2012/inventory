<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logo extends Model
{
    public $timestamps = false;
    protected $table = 'tbl_logo';
    protected $primaryKey = 'logo_id';
    protected $fillable = ['logo'];
}
