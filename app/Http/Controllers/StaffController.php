<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Staff;
use DB;
use Session;

class StaffController extends Controller
{
    
    private function check_permission() {
        $admin = session::get('admin_login');
        if($admin == true){
            return true;
        } else {
            return false;
        }
    }
    
    public function staff_form(){
        if(!$this->check_permission()){
            session::flash('message', error_message("Permission denied or session expired"));
            return redirect()->action('HomeController@index');
        }
        $title = "Staff";
        $staffs = DB::table('tbl_staff')
                ->orderBy('staff_id', 'desc')
                ->paginate(10);
        return view('staff.staff_form', [
            'title' => $title,
            'staffs' => $staffs
        ]);
    }
    
    public function add_staff(Request $request){
        if(!$this->check_permission()){
            session::flash('message', error_message("Permission denied or session expired"));
            return redirect()->action('HomeController@index');
        }
        $staff = new Staff;
        $staff->staff_name = $staff_name = trim($request->staff_name);
        $staff->staff_phone = trim($request->staff_phone);
        $staff->staff_email = trim($request->staff_email);
        $staff->staff_password = sha1(trim($request->staff_password));
        $staff->staff_address = trim($request->staff_address);
        $staff->entry_time = date('Y-m-d H:i:s');
        
        $check_staff = DB::table('tbl_staff')
                ->where('staff_name', $staff_name)
                ->count();
        if($check_staff > 0){
            session::flash('message', error_message("Staff name already exist"));
            return redirect()->action('StaffController@staff_form');
        } else {
            $staff->save();
            session::flash('message', success_message("Staff name added successfully"));
            return redirect()->action('StaffController@staff_form');
        }
    }
    
    public function view_staff($staff_id){
        if(!$this->check_permission()){
            session::flash('message', error_message("Permission denied or session expired"));
            return redirect()->action('HomeController@index');
        }
        $staff = DB::table('tbl_staff')
                ->where('staff_id', $staff_id)
                ->first();
        $title = $staff->staff_name . " - Staff info";
        return view('staff.view_staff', [
            'title' => $title,
            'staff' => $staff
        ]);
    }
    
    public function delete_staff($staff_id){
        if(!$this->check_permission()){
            session::flash('message', error_message("Permission denied or session expired"));
            return redirect()->action('HomeController@index');
        }
        $staff = Staff::find($staff_id);
        /*
         * Have to build purchage and sell check
         * Regarding to the staff id
         * check if delete associative information
         * regarding to the staff
         * 
         */
        $staff->delete();
        session::flash('message', success_message("Staff deleted successfully"));
        return redirect()->action('StaffController@staff_form');
    }
    
    public function edit_staff($staff_id){
        if(!$this->check_permission()){
            session::flash('message', error_message("Permission denied or session expired"));
            return redirect()->action('HomeController@index');
        }
        $staff = Staff::find($staff_id);
        $title = "Update Staff";
        return view('staff.edit_staff', ['staff' => $staff, 'title' => $title]);
    }
    
    public function update_staff(Request $request){
        if(!$this->check_permission()){
            session::flash('message', error_message("Permission denied or session expired"));
            return redirect()->action('HomeController@index');
        }
        $staff_id = $request->staff_id; 
        
        $staff = Staff::find($staff_id);
        $staff_name = trim($request->staff_name);
        $staff->staff_phone = trim($request->staff_phone);
        $staff->staff_email = trim($request->staff_email);
        $staff->staff_password = sha1(trim($request->staff_password));
        $staff->staff_address = trim($request->staff_address);
        
        $check_staff = DB::table('tbl_staff')
                ->where('staff_name', $staff_name)
                ->count();
        if($check_staff > 0){
            session::flash('message', error_message("All data updated except name. Name already exist"));
            $staff->save();
            return redirect()->action('StaffController@edit_staff', ['staff_id' => $staff_id]);
        } else {
            $staff->staff_name = $staff_name;
            $staff->save();
            session::flash('message', success_message("Staff name updated successfully"));
            return redirect()->action('StaffController@staff_form');
        }
    }
    
    public function updateActivePerm($staff_id, $perm){
        if(!$this->check_permission()){
            session::flash('message', error_message("Permission denied or session expired"));
            return redirect()->action('HomeController@index');
        }
        DB::table('tbl_staff')->where('staff_id', $staff_id)
                ->update([$perm => 1]);
        return "true";
    }
    
    public function updateInactivePerm($staff_id, $perm){
        if(!$this->check_permission()){
            session::flash('message', error_message("Permission denied or session expired"));
            return redirect()->action('HomeController@index');
        }
        DB::table('tbl_staff')->where('staff_id', $staff_id)
                ->update([$perm => 0]);
        return "true";
    }
    
    
    
}
