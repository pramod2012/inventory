<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Logo;
use DB;
use Session;
class LogoController extends Controller
{
    public function logo(){
        $site_logo = "Update app logo";
        $count_logo = DB::table('tbl_logo')->count();
        if($count_logo == 1){
            $current_logo = DB::table('tbl_logo')
                    ->orderBy('logo_id', 'desc')
                    ->first();
        } else {
            $current_logo = "";
        }
        return view('config.logo', ['logo' => $site_logo, 'count' => $count_logo, 'current_logo' => $current_logo]);
    }
    
    public function save_logo(Request $request){
        $logo = new Logo;
        
        $file = $request->file('logo');
        $destinationPath = 'asset/images/logo';
        if($file->move($destinationPath, $file->getClientOriginalName())){
            $logoimage = $destinationPath . '/' . $file->getClientOriginalName();
        }
        $logo->logo = $logoimage;
        $logo->save();
        Session::flash('message', success_message("App logo added successfully"));
        return redirect()->action('LogoController@logo');
    }
    
    public function update_logo(Request $request){
        $logo_id = $request->logo_id;
        $logo = Logo::find($logo_id);
        $logo_image = $logo->logo;
        unlink($logo_image);
        
        $file = $request->file('logo');
        $destinationPath = 'asset/images/logo';
        if($file->move($destinationPath, $file->getClientOriginalName())){
            $logoimage = $destinationPath . '/' . $file->getClientOriginalName();
        }
        $logo->logo = $logoimage;
        $logo->save();
        Session::flash('message', success_message("App logo added successfully"));
        return redirect()->action('LogoController@logo');
    }
    
    
    
}
