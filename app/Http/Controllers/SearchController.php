<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Session;

class SearchController extends Controller
{
    public function search(Request $request){
        $category_id = $this->setInput($request->category_id);
        $brand_id = $this->setInput($request->brand_id);
        $product_id = $this->setInput($request->product_id);
        $ce = $be = $pe = '=';
        if(!$category_id){
            $ce = "!=";
            $category_id = 0;
        }
        if(!$brand_id){
            $be = "!=";
            $brand_id = 0;
        }
        if(!$product_id){
            $pe = "!=";
            $product_id = 0;
        }
        $results = DB::table('tbl_stock')
                ->where('tbl_stock.category_id', $ce, $category_id)
                ->where('tbl_stock.brand_id', $be, $brand_id)
                ->where('tbl_stock.product_id', $pe, $product_id)
                ->where('quantity_remains', '>', 0)
                ->join('tbl_category', 'tbl_stock.category_id', '=', 'tbl_category.category_id')
                ->join('tbl_brand', 'tbl_stock.brand_id', '=', 'tbl_brand.brand_id')
                ->join('tbl_product', 'tbl_stock.product_id', '=', 'tbl_product.product_id')
                ->paginate(20);
        $count = DB::table('tbl_stock')
                ->where('tbl_stock.category_id', $ce, $category_id)
                ->where('tbl_stock.brand_id', $be, $brand_id)
                ->where('tbl_stock.product_id', $pe, $product_id)
                ->where('quantity_remains', '>', 0)
                ->count();
        $title = "Total result found " . $count;
        return view('search.search_result', [
           'title' => $title,
           'results' => $results
        ]);
    }
    
    private function setInput($var){
        if(isset($var) && !empty($var)){
            return $var;
        } else {
            return false;
        }
    }
    
    
}
