<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Supplier;
use DB;
use Session;

class SupplierController extends Controller
{
    
    private function check_permission() {
        $admin = session::get('admin_login');
        $staff = session::get('staff_login');
        if($admin == true or $staff == true){
            if($staff == true){
                $staff_id = session::get('staff_id');
                $perm = DB::table('tbl_staff')
                        ->where('staff_id', $staff_id)
                        ->first();
                $permission = $perm->supplier_perm;
                if($permission == 1){
                    return true;
                } else {
                    return false;
                }
            } 
            if($admin == true){
                return true;
            }
        } else {
            return false;
        }
    }
    
    
    public function supplier_form(){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $title = "Supplier";
        $suppliers = DB::table('tbl_supplier')
                ->where('supplier_id', '!=', 1)
                ->orderBy('supplier_id', 'desc')
                ->paginate(10);
        $all_suppliers = DB::table('tbl_supplier')
                ->where('supplier_id', '!=', 1)
                ->get();
        return view('supplier.supplier_form', [
            'title' => $title,
            'suppliers' => $suppliers,
            'all_suppliers' => $all_suppliers
        ]);
    }
    
    public function add_supplier(Request $request){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $supplier = new Supplier;
        $supplier->supplier_name = $supplier_name = trim($request->supplier_name);
        $supplier->supplier_description = trim($request->supplier_description);
        $supplier->supplier_phone = trim($request->supplier_phone);
        $supplier->supplier_email = trim($request->supplier_email);
        $supplier->supplier_website = trim($request->supplier_website);
        $supplier->supplier_address = trim($request->supplier_address);
        $supplier->entry_time = date('Y-m-d H:i:s');
        
        $check_supplier = DB::table('tbl_supplier')
                ->where('supplier_name', $supplier_name)
                ->count();
        if($check_supplier > 0){
            session::flash('message', error_message("Supplier name already exist"));
            return redirect()->action('SupplierController@supplier_form');
        } else {
            $supplier->save();
            session::flash('message', success_message("Supplier name added successfully"));
            return redirect()->action('SupplierController@supplier_form');
        }
    }
    
    public function view_supplier($supplier_id){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $supplier = DB::table('tbl_supplier')
                ->where('supplier_id', $supplier_id)
                ->first();
        $title = $supplier->supplier_name . " - Supplier info";
        return view('supplier.view_supplier', [
            'title' => $title,
            'supplier' => $supplier
        ]);
    }
    
    public function delete_supplier($supplier_id){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $supplier = Supplier::find($supplier_id);
        DB::table('tbl_stock')
                ->where('supplier_id', $supplier_id)
                ->update(['supplier' => 1]);
        $supplier->delete();
        session::flash('message', success_message("Supplier deleted successfully"));
        return redirect()->action('SupplierController@supplier_form');
    }
    
    public function edit_supplier($supplier_id){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $supplier = Supplier::find($supplier_id);
        $title = "Update Supplier";
        return view('supplier.edit_supplier', ['supplier' => $supplier, 'title' => $title]);
    }
    
    public function update_supplier(Request $request){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $supplier_id = $request->supplier_id; 
        
        $supplier = Supplier::find($supplier_id);
        $supplier->supplier_name = $supplier_name = trim($request->supplier_name);
        $supplier->supplier_description = trim($request->supplier_description);
        $supplier->supplier_phone = trim($request->supplier_phone);
        $supplier->supplier_email = trim($request->supplier_email);
        $supplier->supplier_website = trim($request->supplier_website);
        $supplier->supplier_address = trim($request->supplier_address);
        
        $check_supplier = DB::table('tbl_supplier')
                ->where('supplier_name', $supplier_name)
                ->count();
        if($check_supplier > 0){
            session::flash('message', error_message("This supplier name already exist"));
            return redirect()->action('SupplierController@edit_supplier', ['supplier_id' => $supplier_id]);
        } else {
            $supplier->save();
            session::flash('message', success_message("Supplier name updated successfully"));
            return redirect()->action('SupplierController@supplier_form');
        }
    }
}
