<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Session;
use DB;

class SettingsController extends Controller
{
    public function index(){
        $title = "Settings";
        return view('settings.settings_home', ['title' => $title]);
    }
    
    public function update_password(Request $request){
        $old_pass = $request->old_pass;
        $new_pass = $request->new_pass;
        $con_new_pass = $request->con_new_pass;
        $admin_id = Session::get('admin_id');
        if($new_pass !== $con_new_pass){
            Session::flash('message', error_message("New password and confirm password not match"));
            return redirect()->action('SettingsController@index');
        }
        DB::table('tbl_admin')
                ->where('admin_id', $admin_id)
                ->where('password', sha1($old_pass))
                ->update(['password' => sha1($new_pass)]);
        Session::flash('message', success_message("Password updated successfully"));
        return redirect()->action('SettingsController@index');
    }
}
