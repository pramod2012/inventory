<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Session;

class HomeController extends Controller
{
    public function index(){
        if(session::get('admin_login') == 1  || session::get('staff_login') == 1){
            return redirect()->action('HomeController@home');
        }
        return view('login');
    }
    
    public function login(Request $request){
        $email = $request->email;
        $password = $request->password;
        
        $admin = DB::table('tbl_admin')
                ->where([
                    ['email', $email],
                    ['password', sha1($password)]
                ])->first();
        $staff = DB::table('tbl_staff')
                ->where([
                    ['staff_email', $email],
                    ['staff_password', sha1($password)]
                ])->first();
        if($admin){
            $admin_id = $admin->admin_id;
            $admin_login = TRUE;
            
            session::set('admin_id', $admin_id);
            session::set('admin_login', $admin_login);
            
            return redirect()->action('HomeController@home');
        } else {
            if($staff){
                $staff_id = $staff->staff_id;
                $staff_login = TRUE;

                session::set('staff_id', $staff_id);
                session::set('staff_login', $staff_login);

                return redirect()->action('HomeController@home');
            } else {
                session::flash('message', success_message("Invalid Email/Password"));
                return redirect()->action('HomeController@index');
            }
        }
        
        
    }
    
    public function logout(){
        session::flush();
        
        session::flash('message', success_message("Logged out successfully"));
        return redirect()->action('HomeController@index');
    }
    
    public function home(){
        $admin = session::get('admin_login');
        $staff = session::get('staff_login');
        if(!$admin){
            if(!$staff){
                return redirect()->action('HomeController@index');
            }
        }
        $categories = DB::table('tbl_category')->get();
        $brands = DB::table('tbl_brand')->get();
        $products = DB::table('tbl_product')->get();
        return view('home_msg', ['categories' => $categories, 'brands' => $brands, 'products' => $products]);
    }
    
    public function access_denied(){
        return view('permission.denied');
    }
    
    
    
    
}
