<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Session;
use DB;
use App\Stock;

class StockController extends Controller
{
    private function check_permission() {
        $admin = session::get('admin_login');
        $staff = session::get('staff_login');
        if($admin == true or $staff == true){
            if($staff == true){
                $staff_id = session::get('staff_id');
                $perm = DB::table('tbl_staff')
                        ->where('staff_id', $staff_id)
                        ->first();
                $permission = $perm->stock_perm;
                if($permission == 1){
                    return true;
                } else {
                    return false;
                }
            } 
            if($admin == true){
                return true;
            }
        } else {
            return false;
        }
    }
    
    public function due_pay_form($supplier_id){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $supplier = DB::table('tbl_supplier')
                ->where('supplier_id', $supplier_id)->first();
        $supplier_name = $supplier->supplier_name;
        
        $pay_due = DB::table('tbl_stock')
                ->where('supplier_id', $supplier_id)
                ->sum('pay_due');
        
        $price_total = DB::table('tbl_stock')
                ->where('supplier_id', $supplier_id)
                ->sum('total_price');
        
        $price_paid = DB::table('tbl_stock')
                ->where('supplier_id', $supplier_id)
                ->sum('stock_paid_amount');
        $due_total = $price_total - ($price_paid + $pay_due);
        $title = "Pay Due";
        return view('stock.due_pay_form', [
            'title' => $title,
            'supplier' => $supplier_name,
            'supplier_id' => $supplier->supplier_id,
            'due_total' => $due_total
                ]);
    }
    
    public function save_due(Request $request){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $staff = session::get('staff_login');
        if($staff == true){
            $staff_id = session::get('staff_id');
        } else {
            $staff_id = 0;
        }
        $stock = new Stock;
        $stock->supplier_id = $request->supplier_id;
        $stock->due_total = $request->due_total;
        $stock->pay_due = $request->pay_due;
        $stock->stock_date = date('Y-m-d H:i:s');
        $stock->added_by = $staff_id;
        $stock->entry_time = date('Y-m-d H:i:s');
        $stock->save();
        //Session::flash('message', success_message(""));
        return view('stock.due_pay_report', ['stock' => $stock]);
    }
    
    public function stock_form(){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $title = "Stock Form";
        $suppliers = DB::table('tbl_supplier')
                ->where('supplier_id', '!=', 1)
                ->get();
        $categories = DB::table('tbl_category')
                ->where('category_id', '!=', 1)
                ->get();
        $brands = DB::table('tbl_brand')
                ->where('brand_id', '!=', 1)
                ->get();
        $products = DB::table('tbl_product')
                ->where('product_id', '!=', 1)
                ->get();
        return view('stock.stock_form', [
            'title' => $title, 
            'suppliers' => $suppliers,
            'categories' => $categories,
            'brands' => $brands,
            'products' => $products
                ]);
    }
    
    public function stock_history(){
        $stocks = DB::table('tbl_stock')
                ->join('tbl_category', 'tbl_stock.category_id', '=', 'tbl_category.category_id')
                ->join('tbl_brand', 'tbl_stock.brand_id', '=', 'tbl_brand.brand_id')
                ->join('tbl_product', 'tbl_stock.product_id', '=', 'tbl_product.product_id')
                ->join('tbl_supplier', 'tbl_stock.supplier_id', '=', 'tbl_supplier.supplier_id')
                ->paginate(20);
        $title = "Stock history";
        return view('stock.stock_history', [
                'title'=>$title,
                'stocks' => $stocks
                ]);
    }
    
    public function available_stock(){
        $stocks = DB::table('tbl_stock')
                ->where('quantity_remains', '>', 0)
                ->join('tbl_category', 'tbl_stock.category_id', '=', 'tbl_category.category_id')
                ->join('tbl_brand', 'tbl_stock.brand_id', '=', 'tbl_brand.brand_id')
                ->join('tbl_product', 'tbl_stock.product_id', '=', 'tbl_product.product_id')
                ->join('tbl_supplier', 'tbl_stock.supplier_id', '=', 'tbl_supplier.supplier_id')
                ->paginate(20);
        $title = "Available stock";
        return view('stock.available_stock', [
                'title'=>$title,
                'stocks' => $stocks
                ]);
    }
    
    public function save_stock(Request $request){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $staff = session::get('staff_login');
        if($staff == true){
            $staff_id = session::get('staff_id');
        } else {
            $staff_id = 0;
        }
        
        $stock = new Stock;
        $stock->category_id = $request->category_id;
        $stock->brand_id = $request->brand_id;
        $stock->product_id = $request->product_id;
        $stock->supplier_id = $request->supplier_id;
        $stock->stock_date = $request->stock_date;
        $stock->stock_quantity = $stock->quantity_remains= $request->stock_quantity;
        $stock->stock_rate = $request->stock_rate;
        $stock->total_price = $total_price = $stock->stock_quantity * $stock->stock_rate;
        $stock->stock_paid_amount = $paid = $request->stock_paid_amount;
        $stock->stock_due_amount = $total_price - $paid;
        $stock->added_by = $staff_id;
        $stock->entry_time = date('Y-m-d H:i:s');
        $stock->save();
        Session::flash('message', success_message("Stock added successfully"));
        return view('stock.stock_report', ['stock' => $stock]);
    }
    
    public function check_stocks($category_id, $brand_id, $product_id){
        $count = DB::table('tbl_stock')
                ->where([
                    ['tbl_stock.category_id', $category_id],
                    ['tbl_stock.brand_id', $brand_id],
                    ['tbl_stock.product_id', $product_id],
                    ['quantity_remains', '>', 0]
                ])
                ->join('tbl_category', 'tbl_stock.category_id', '=', 'tbl_category.category_id')
                ->join('tbl_brand', 'tbl_stock.brand_id', '=', 'tbl_brand.brand_id')
                ->join('tbl_product', 'tbl_stock.product_id', '=', 'tbl_product.product_id')
                ->count();
        
        if($count > 0){
            $result = DB::table('tbl_stock')
                    ->where([
                        ['tbl_stock.category_id', $category_id],
                        ['tbl_stock.brand_id', $brand_id],
                        ['tbl_stock.product_id', $product_id],
                        ['quantity_remains', '>', 0]
                    ])
                    ->join('tbl_category', 'tbl_stock.category_id', '=', 'tbl_category.category_id')
                    ->join('tbl_brand', 'tbl_stock.brand_id', '=', 'tbl_brand.brand_id')
                    ->join('tbl_product', 'tbl_stock.product_id', '=', 'tbl_product.product_id')
                    ->sum('quantity_remains');
            return $result;
        } else {
            return 0;
        }
    }
    
    
    
}
