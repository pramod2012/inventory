<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Session;

class StateController extends Controller
{
    public function buyer_history_all($buyer_id){
        $history = DB::table('tbl_sell')
                ->where('tbl_sell.buyer_id', $buyer_id)
                ->leftJoin('tbl_category', 'tbl_sell.category_id', '=', 'tbl_category.category_id')
                ->leftJoin('tbl_brand', 'tbl_sell.brand_id', '=', 'tbl_brand.brand_id')
                ->leftJoin('tbl_product', 'tbl_sell.product_id', '=', 'tbl_product.product_id')
                ->leftJoin('tbl_buyer', 'tbl_sell.buyer_id', '=', 'tbl_buyer.buyer_id')
                ->get();
        if(count($history) > 0){
        $title = $history[0]->buyer_name;
        } else {
            $title ="No data available";
        }
        return view('state.buyer_history', ['history' => $history, 'title' => $title]);
    }
    
    public function buyer_history(Request $request){
        $buyer_id = $request->buyer_id;
        if(empty($buyer_id)){
            Session::flash('m', error_message("Invalid buyer name"));
            return redirect()->action('BuyerController@buyer_form');
        }
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        if(empty($from_date) || empty($to_date)){
            Session::flash('m', error_message("From and To date required"));
            return redirect()->action('BuyerController@buyer_form');
        }
        $history = DB::table('tbl_sell')
                ->where([
                    ['tbl_sell.buyer_id', '=', $buyer_id],
                    ['tbl_sell.sell_date', '>=', $from_date],
                    ['tbl_sell.sell_date', '<=', $to_date]
                ])
                ->leftJoin('tbl_category', 'tbl_sell.category_id', '=', 'tbl_category.category_id')
                ->leftJoin('tbl_brand', 'tbl_sell.brand_id', '=', 'tbl_brand.brand_id')
                ->leftJoin('tbl_product', 'tbl_sell.product_id', '=', 'tbl_product.product_id')
                ->leftJoin('tbl_buyer', 'tbl_sell.buyer_id', '=', 'tbl_buyer.buyer_id')
                ->get();
        if(count($history) > 0){
        $title = $history[0]->buyer_name;
        return view('state.buyer_history', [
            'history' => $history, 
            'title' => $title,
            'from' => $from_date,
            'to' => $to_date
                ]);
        } else {
            $title = "No data available";
            return view('state.buyer_history', [
            'history' => $history, 
            'title' => $title,
            'from' => $from_date,
            'to' => $to_date
                ]);
        }
    }
    
    
    public function supplier_history_all($supplier_id){
        $history = DB::table('tbl_stock')
                ->where('tbl_stock.supplier_id', $supplier_id)
                ->leftJoin('tbl_category', 'tbl_stock.category_id', '=', 'tbl_category.category_id')
                ->leftJoin('tbl_brand', 'tbl_stock.brand_id', '=', 'tbl_brand.brand_id')
                ->leftJoin('tbl_product', 'tbl_stock.product_id', '=', 'tbl_product.product_id')
                ->leftJoin('tbl_supplier', 'tbl_stock.supplier_id', '=', 'tbl_supplier.supplier_id')
                ->get();
        $title = $history[0]->supplier_name;
        return view('state.supplier_history', ['history' => $history, 'title' => $title]);
    }
    
    public function supplier_history(Request $request){
        $supplier_id = $request->supplier_id;
        if(empty($supplier_id)){
            Session::flash('m', error_message("Invalid supplier name"));
            return redirect()->action('SupplierController@supplier_form');
        }
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        if(empty($from_date) || empty($to_date)){
            Session::flash('m', error_message("From and To date required"));
            return redirect()->action('SupplierController@supplier_form');
        }
        $history = DB::table('tbl_stock')
                ->where([
                    ['tbl_stock.supplier_id', '=', $supplier_id],
                    ['tbl_stock.stock_date', '>=', $from_date],
                    ['tbl_stock.stock_date', '<=', $to_date]
                ])
                ->leftJoin('tbl_category', 'tbl_stock.category_id', '=', 'tbl_category.category_id')
                ->leftJoin('tbl_brand', 'tbl_stock.brand_id', '=', 'tbl_brand.brand_id')
                ->leftJoin('tbl_product', 'tbl_stock.product_id', '=', 'tbl_product.product_id')
                ->leftJoin('tbl_supplier', 'tbl_stock.supplier_id', '=', 'tbl_supplier.supplier_id')
                ->get();
        if(count($history) > 0) {
        $title = $history[0]->supplier_name;
        return view('state.supplier_history', [
            'history' => $history, 
            'title' => $title,
            'from' => $from_date,
            'to' => $to_date
                ]);
        } else {
            $title = "No data Available";
        return view('state.supplier_history', [
            'history' => $history, 
            'title' => $title,
            'from' => $from_date,
            'to' => $to_date
                ]);
        }
    }
    
    
    
    
    
}
