<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Sell;
use DB;
use Session;

class SellController extends Controller
{
    private function check_permission() {
        $admin = session::get('admin_login');
        $staff = session::get('staff_login');
        if($admin == true or $staff == true){
            if($staff == true){
                $staff_id = session::get('staff_id');
                $perm = DB::table('tbl_staff')
                        ->where('staff_id', $staff_id)
                        ->first();
                $permission = $perm->sell_perm;
                if($permission == 1){
                    return true;
                } else {
                    return false;
                }
            } 
            if($admin == true){
                return true;
            }
        } else {
            return false;
        }
    }
    
    public function due_receive_form($buyer_id){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $buyer = DB::table('tbl_buyer')
                ->where('buyer_id', $buyer_id)->first();
        $buyer_name = $buyer->buyer_name;
        
        $due_total = DB::table('tbl_sell')
                ->where('buyer_id', $buyer_id)
                ->sum('sell_due_price');
        
        $receive_due = DB::table('tbl_sell')
                ->where('buyer_id', $buyer_id)
                ->sum('receive_due');
        $receivable = $due_total - $receive_due;
        
        $title = "Receive Due";
        return view('sell.due_receive_form', [
            'title' => $title,
            'buyer' => $buyer_name,
            'buyer_id' => $buyer->buyer_id,
            'due_total' => $receivable
                ]);
    }
    
    public function save_due(Request $request){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $staff = session::get('staff_login');
        if($staff == true){
            $staff_id = session::get('staff_id');
        } else {
            $staff_id = 0;
        }
        $sell = new Sell;
        $sell->buyer_id = $request->buyer_id;
        $sell->due_total = $request->due_total;
        $sell->receive_due = $request->receive_due;
        $sell->sell_date = $request->sell_date;
        $sell->added_by = $staff_id;
        $sell->entry_time = date('Y-m-d H:i:s');
        $sell->save();
        //Session::flash('message', success_message(""));
        return view('sell.due_report', ['sell' => $sell]);
        
    }
    
    
    public function sell_form(){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $title = "Sell Form";
        $buyers = DB::table('tbl_buyer')
                ->where('buyer_id', '!=', 1)
                ->get();
        $categories = DB::table('tbl_category')
                ->where('category_id', '!=', 1)
                ->get();
        $brands = DB::table('tbl_brand')
                ->where('brand_id', '!=', 1)
                ->get();
        $products = DB::table('tbl_product')
                ->where('product_id', '!=', 1)
                ->get();
        return view('sell.sell_form', [
            'title' => $title, 
            'buyers' => $buyers,
            'categories' => $categories,
            'brands' => $brands,
            'products' => $products
                ]);
    }
    
    public function sell_history(){
        $sells = DB::table('tbl_sell')
                ->join('tbl_category', 'tbl_sell.category_id', '=', 'tbl_category.category_id')
                ->join('tbl_brand', 'tbl_sell.brand_id', '=', 'tbl_brand.brand_id')
                ->join('tbl_product', 'tbl_sell.product_id', '=', 'tbl_product.product_id')
                ->join('tbl_buyer', 'tbl_sell.buyer_id', '=', 'tbl_buyer.buyer_id')
                ->paginate(20);
        $title = "Sell history";
        return view('sell.sell_history', [
                'title'=>$title,
                'sells' => $sells
                ]);
    }
    
    
    
    public function save_sell(Request $request){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $staff = session::get('staff_login');
        if($staff == true){
            $staff_id = session::get('staff_id');
        } else {
            $staff_id = 0;
        }
        
        $sell = new Sell;
        $sell->category_id = $category_id= $request->category_id;
        $sell->brand_id = $brand_id = $request->brand_id;
        $sell->product_id = $product_id = $request->product_id;
        $sell->buyer_id = $request->buyer_id;
        $sell->sell_quantity = $qty = $request->sell_quantity;
        $sell->sell_rate = $rate =  $request->sell_rate;
        $sell->sell_total_price = $qty * $rate;
        $sell->sell_received_price = $request->sell_received_price;
        $sell->sell_due_price = $sell->sell_total_price - $sell->sell_received_price;
        $sell->sell_date = $request->sell_date;
        $sell->added_by = $staff_id;
        $sell->entry_time = date('Y-m-d H:i:s');
        $sell_as = $request->sell_type;
        $available = $this->check_stock($category_id, $brand_id, $product_id);
        if($available > 0){
            if($available > $qty){
                //update stock table
                
                if($sell_as == "f"){
                    $stocks = $this->return_stocks($category_id, $brand_id, $product_id);
                    foreach($stocks as $stock){
                        if($stock->quantity_remains > $qty){
                            $new_remains = $stock->quantity_remains - $qty;
                            DB::table('tbl_stock')
                                ->where('stock_id', $stock->stock_id)
                                ->update(['quantity_remains' => $new_remains]);
                            break;
                        } else if($stock->quantity_remains < $qty){
                            DB::table('tbl_stock')
                                ->where('stock_id', $stock->stock_id)
                                ->update(['quantity_remains' => 0]);
                            $qty = $qty - $stock->quantity_remains;
                        }
                        
                    }
                } else if($sell_as == "l"){
                    $stocks = $this->return_stocks_desc($category_id, $brand_id, $product_id);
                    foreach($stocks as $stock){
                        if($stock->quantity_remains > $qty){
                            $new_remains = $stock->quantity_remains - $qty;
                            DB::table('tbl_stock')
                                ->where('stock_id', $stock->stock_id)
                                ->update(['quantity_remains' => $new_remains]);
                            break;
                        } else if($stock->quantity_remains < $qty){
                            DB::table('tbl_stock')
                                ->where('stock_id', $stock->stock_id)
                                ->update(['quantity_remains' => 0]);
                            $qty = $qty - $stock->quantity_remains;
                        }
                        
                    }
                    //end update table
                }
                $sell->save();
                Session::flash('message', success_message("Sell added successfully. Updated stock table"));
                return view('sell.sell_report', ['sell' => $sell]);
            } else {
                Session::flash('message', error_message("This much product is not available. Total Stock: " . $available));
                return redirect()->action('SellController@sell_form');
            }
        } else {
            Session::flash('message', error_message("This product is not available"));
            return redirect()->action('SellController@sell_form');
        }
        
    }
    
    private function return_stocks($cid, $bid, $pid){
        $stocks = DB::table('tbl_stock')
                ->where('category_id', $cid)
                ->where('brand_id', $bid)
                ->where('product_id', $pid)
                ->get();
        return $stocks;
    }
    
    private function return_stocks_desc($cid, $bid, $pid){
        $stocks = DB::table('tbl_stock')
                ->where('category_id', $cid)
                ->where('brand_id', $bid)
                ->where('product_id', $pid)
                ->orderBy('stock_id', 'desc')
                ->get();
        return $stocks;
    }
    
    private function check_stock($cid, $bid, $pid){
        $count = DB::table('tbl_stock')
                ->where('category_id', $cid)
                ->where('brand_id', $bid)
                ->where('product_id', $pid)
                ->sum('tbl_stock.quantity_remains');
        return $count;
    }
    
    
    
    
    
}
