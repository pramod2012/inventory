<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Brand;
use DB;
use Session;

class BrandController extends Controller
{
    private function check_permission() {
        $admin = session::get('admin_login');
        $staff = session::get('staff_login');
        if($admin == true or $staff == true){
            if($staff == true){
                $staff_id = session::get('staff_id');
                $perm = DB::table('tbl_staff')
                        ->where('staff_id', $staff_id)
                        ->first();
                $permission = $perm->brand_perm;
                if($permission == 1){
                    return true;
                } else {
                    return false;
                }
            } 
            if($admin == true){
                return true;
            }
        } else {
            return false;
        }
    }
    
    public function brand_form(){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $title = "Brand";
        $brands = DB::table('tbl_brand')
                ->where('brand_id', '!=', 1)
                ->orderBy('brand_id', 'desc')
                ->paginate(10);
        $all_brands = DB::table('tbl_brand')
                ->where('brand_id', '!=', 1)
                ->get();
        return view('brand.brand_form', [
            'title' => $title,
            'brands' => $brands,
            'all_brands' => $all_brands
        ]);
    }
    
    public function search_brand($brand_id){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $brands = DB::table('tbl_stock')
                ->where('tbl_stock.brand_id', $brand_id)
                ->join('tbl_category', 'tbl_stock.category_id', '=', 'tbl_category.category_id')
                ->join('tbl_brand', 'tbl_stock.brand_id', '=', 'tbl_brand.brand_id')
                ->join('tbl_product', 'tbl_stock.product_id', '=', 'tbl_product.product_id')
                ->join('tbl_supplier', 'tbl_stock.supplier_id', '=', 'tbl_supplier.supplier_id')
                ->paginate(20);
        if(count($brands) > 1){
        $title = $brands[0]->brand_name;
        } else {
            $title = "No item found in this brand";
        }
        return view('brand.brand_history', [
            'title' => $title,
            'brands' => $brands
        ]);
    }
    
    public function add_brand(Request $request){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $brand = new Brand;
        $brand->brand_name = $brand_name = trim($request->brand_name);
        $brand->brand_description = trim($request->brand_description);
        $brand->entry_time = date('Y-m-d H:i:s');
        
        $check_brand = DB::table('tbl_brand')
                ->where('brand_name', $brand_name)
                ->count();
        if($check_brand > 0){
            session::flash('message', error_message("Brand name already exist"));
            return redirect()->action('BrandController@brand_form');
        } else {
            $brand->save();
            session::flash('message', success_message("Brand name added successfully"));
            return redirect()->action('BrandController@brand_form');
        }
    }
    
    public function delete_brand($brand_id){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $brand = Brand::find($brand_id);
        DB::table('tbl_stock')
                ->where('brand_id', $brand_id)
                ->update(['brand_id' => 1]);
        DB::table('tbl_sell')
                ->where('brand_id', $brand_id)
                ->update(['brand_id' => 1]);
        $brand->delete();
        session::flash('message', success_message("Brand deleted successfully"));
        return redirect()->action('BrandController@brand_form');
    }
    
    public function edit_brand($brand_id){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $brand = Brand::find($brand_id);
        $title = "Update Brand";
        return view('brand.edit_brand', ['brand' => $brand, 'title' => $title]);
    }
    
    public function update_brand(Request $request){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $brand_id = $request->brand_id; 
        
        $brand = Brand::find($brand_id);
        $brand->brand_name = $brand_name = trim($request->brand_name);
        $brand->brand_description = trim($request->brand_description);
        
        $check_brand = DB::table('tbl_brand')
                ->where('brand_name', $brand_name)
                ->count();
        if($check_brand > 0){
            session::flash('message', error_message("This brand name already exist"));
            return redirect()->action('BrandController@edit_brand', ['brand_id' => $brand_id]);
        } else {
            $brand->save();
            session::flash('message', success_message("Brand name updated successfully"));
            return redirect()->action('BrandController@brand_form');
        }
    }
}
