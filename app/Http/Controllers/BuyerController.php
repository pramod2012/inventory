<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Buyer;
use DB;
use Session;

class BuyerController extends Controller
{
    private function check_permission() {
        $admin = session::get('admin_login');
        $staff = session::get('staff_login');
        if($admin == true or $staff == true){
            if($staff == true){
                $staff_id = session::get('staff_id');
                $perm = DB::table('tbl_staff')
                        ->where('staff_id', $staff_id)
                        ->first();
                $permission = $perm->buyer_perm;
                if($permission == 1){
                    return true;
                } else {
                    return false;
                }
            } 
            if($admin == true){
                return true;
            }
        } else {
            return false;
        }
    }
    
    public function buyer_form(){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $title = "Buyer";
        $buyers = DB::table('tbl_buyer')
                ->where('buyer_id', '!=', 1)
                ->orderBy('buyer_id', 'desc')
                ->paginate(10);
        $all_buyers = DB::table('tbl_buyer')
                ->where('buyer_id', '!=', 1)
                ->get();
        return view('buyer.buyer_form', [
            'title' => $title,
            'buyers' => $buyers,
            'all_buyers' => $all_buyers
        ]);
    }
    
    public function add_buyer(Request $request){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $buyer = new Buyer;
        $buyer->buyer_name = $buyer_name = trim($request->buyer_name);
        $buyer->buyer_description = trim($request->buyer_description);
        $buyer->buyer_phone = trim($request->buyer_phone);
        $buyer->buyer_email = trim($request->buyer_email);
        $buyer->buyer_website = trim($request->buyer_website);
        $buyer->buyer_address = trim($request->buyer_address);
        $buyer->entry_time = date('Y-m-d H:i:s');
        
        $check_buyer = DB::table('tbl_buyer')
                ->where('buyer_name', $buyer_name)
                ->count();
        if($check_buyer > 0){
            session::flash('message', error_message("Buyer name already exist"));
            return redirect()->action('BuyerController@buyer_form');
        } else {
            $buyer->save();
            session::flash('message', success_message("Buyer name added successfully"));
            return redirect()->action('BuyerController@buyer_form');
        }
    }
    
    public function view_buyer($buyer_id){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $buyer = DB::table('tbl_buyer')
                ->where('buyer_id', $buyer_id)
                ->first();
        $title = $buyer->buyer_name . " - Buyer info";
        return view('buyer.view_buyer', [
            'title' => $title,
            'buyer' => $buyer
        ]);
    }
    
    public function delete_buyer($buyer_id){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $buyer = Buyer::find($buyer_id);
        DB::table('tbl_sell')
                ->where('buyer_id', $buyer_id)
                ->update(['buyer_id' => 1]);
        $buyer->delete();
        session::flash('message', success_message("Buyer deleted successfully"));
        return redirect()->action('BuyerController@buyer_form');
    }
    
    public function edit_buyer($buyer_id){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $buyer = Buyer::find($buyer_id);
        $title = "Update Buyer";
        return view('buyer.edit_buyer', ['buyer' => $buyer, 'title' => $title]);
    }
    
    public function update_buyer(Request $request){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $buyer_id = $request->buyer_id; 
        
        $buyer = Buyer::find($buyer_id);
        $buyer->buyer_name = $buyer_name = trim($request->buyer_name);
        $buyer->buyer_description = trim($request->buyer_description);
        $buyer->buyer_phone = trim($request->buyer_phone);
        $buyer->buyer_email = trim($request->buyer_email);
        $buyer->buyer_website = trim($request->buyer_website);
        $buyer->buyer_address = trim($request->buyer_address);
        
        $check_buyer = DB::table('tbl_buyer')
                ->where('buyer_name', $buyer_name)
                ->count();
        if($check_buyer > 0){
            session::flash('message', error_message("This buyer name already exist"));
            return redirect()->action('BuyerController@edit_buyer', ['buyer_id' => $buyer_id]);
        } else {
            $buyer->save();
            session::flash('message', success_message("Buyer name updated successfully"));
            return redirect()->action('BuyerController@buyer_form');
        }
    }
}
