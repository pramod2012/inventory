<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;
use DB;
use Session;

class CategoryController extends Controller
{
    
    private function check_permission() {
        $admin = session::get('admin_login');
        $staff = session::get('staff_login');
        if($admin == true or $staff == true){
            if($staff == true){
                $staff_id = session::get('staff_id');
                $perm = DB::table('tbl_staff')
                        ->where('staff_id', $staff_id)
                        ->first();
                $permission = $perm->category_perm;
                if($permission == 1){
                    return true;
                } else {
                    return false;
                }
            } 
            if($admin == true){
                return true;
            }
        } else {
            return false;
        }
    }
    
    public function category_form(){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $title = "Category";
        $categories = DB::table('tbl_category')
                ->where('category_id', '!=', 1)
                ->orderBy('category_id', 'desc')
                ->paginate(10);
        $all_categories = DB::table('tbl_category')
                ->where('category_id', '!=', 1)
                ->get();
        return view('category.category_form', [
            'title' => $title,
            'categories' => $categories,
            'all_categories' => $all_categories
        ]);
    }
    
    public function search_category($category_id){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $categories = DB::table('tbl_stock')
                ->where('tbl_stock.category_id', $category_id)
                ->join('tbl_category', 'tbl_stock.category_id', '=', 'tbl_category.category_id')
                ->join('tbl_brand', 'tbl_stock.brand_id', '=', 'tbl_brand.brand_id')
                ->join('tbl_product', 'tbl_stock.product_id', '=', 'tbl_product.product_id')
                ->join('tbl_supplier', 'tbl_stock.supplier_id', '=', 'tbl_supplier.supplier_id')
                ->paginate(20);
        if(count($categories) > 1){
        $title = $categories[0]->category_name;
        } else {
            $title = "No item found in this category";
        }
        return view('category.category_history', [
            'title' => $title,
            'categories' => $categories
        ]);
    }
    
    public function add_category(Request $request){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $category = new Category;
        $category->category_name = $category_name = trim($request->category_name);
        $category->entry_time = date('Y-m-d H:i:s');
        
        $check_category = DB::table('tbl_category')
                ->where('category_name', $category_name)
                ->count();
        if($check_category > 0){
            session::flash('message', error_message("Category name already exist"));
            return redirect()->action('CategoryController@category_form');
        } else {
            $category->save();
            session::flash('message', success_message("Category name added successfully"));
            return redirect()->action('CategoryController@category_form');
        }
    }
    
    public function delete_category($category_id){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $category = Category::find($category_id);
        DB::table('tbl_stock')
                ->where('category_id', $category_id)
                ->update(['category_id' => 1]);
        DB::table('tbl_sell')
                ->where('category_id', $category_id)
                ->update(['category_id' => 1]);
        $category->delete();
        session::flash('message', success_message("Category deleted successfully"));
        return redirect()->action('CategoryController@category_form');
    }
    
    public function edit_category($category_id){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $category = Category::find($category_id);
        $title = "Update Category";
        return view('category.edit_category', ['category' => $category, 'title' => $title]);
    }
    
    public function update_category(Request $request){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $category_id = $request->category_id; 
        
        $category = Category::find($category_id);
        $category->category_name = $category_name = $request->category_name;
        
        $check_category = DB::table('tbl_category')
                ->where('category_name', $category_name)
                ->count();
        if($check_category > 0){
            session::flash('message', error_message("This category name already exist"));
            return redirect()->action('CategoryController@edit_category', ['category_id' => $category_id]);
        } else {
            $category->save();
            session::flash('message', success_message("Category name updated successfully"));
            return redirect()->action('CategoryController@category_form');
        }
    }
    
    
    
    
}
