<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Title;
use DB;
use Session;

class TitleController extends Controller
{
    public function title(){
        $site_title = "Update app title";
        $count_title = DB::table('tbl_title')->count();
        if($count_title == 1){
            $current_title = DB::table('tbl_title')
                    ->orderBy('title_id', 'desc')
                    ->first();
        } else {
            $current_title = "";
        }
        return view('config.title', ['title' => $site_title, 'count' => $count_title, 'current_title' => $current_title]);
    }
    
    public function save_title(Request $request){
        $title = new Title;
        $title->title = $request->title;
        $title->save();
        Session::flash('message', success_message("App title added successfully"));
        return redirect()->action('TitleController@title');
    }
    
    public function update_title(Request $request){
        $title_id = $request->title_id;
        $title = Title::find($title_id);
        
        $title->title = $request->title;
        $title->save();
        Session::flash('message', success_message("App title updated successfullly"));
        return redirect()->action('TitleController@title');
    }
    
    
}
