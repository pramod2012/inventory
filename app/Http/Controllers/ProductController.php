<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;
use DB;
use Session;

class ProductController extends Controller
{
    
    private function check_permission() {
        $admin = session::get('admin_login');
        $staff = session::get('staff_login');
        if($admin == true or $staff == true){
            if($staff == true){
                $staff_id = session::get('staff_id');
                $perm = DB::table('tbl_staff')
                        ->where('staff_id', $staff_id)
                        ->first();
                $permission = $perm->product_perm;
                if($permission == 1){
                    return true;
                } else {
                    return false;
                }
            } 
            if($admin == true){
                return true;
            }
        } else {
            return false;
        }
    }
    
    public function product_form(){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $title = "Products";
        $products = DB::table('tbl_product')
                ->where('product_id', '!=', 1)
                ->orderBy('product_id', 'desc')
                ->paginate(10);
        $all_products = DB::table('tbl_product')
                ->where('product_id', '!=', 1)
                ->get();
        return view('product.product_form', [
            'title' => $title,
            'products' => $products,
            'all_products' => $all_products
        ]);
    }
    
    public function search_product($product_id){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $products = DB::table('tbl_stock')
                ->where('tbl_stock.product_id', $product_id)
                ->join('tbl_category', 'tbl_stock.category_id', '=', 'tbl_category.category_id')
                ->join('tbl_brand', 'tbl_stock.brand_id', '=', 'tbl_brand.brand_id')
                ->join('tbl_product', 'tbl_stock.product_id', '=', 'tbl_product.product_id')
                ->join('tbl_supplier', 'tbl_stock.supplier_id', '=', 'tbl_supplier.supplier_id')
                ->paginate(20);
        $title = $products[0]->product_name;
        return view('product.product_history', [
            'title' => $title,
            'products' => $products
        ]);
    }
    
    public function add_product(Request $request){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $product = new Product;
        $product->product_name = $product_name = trim($request->product_name);
        $product->product_description = trim($request->product_description);
        $product->entry_time = date('Y-m-d H:i:s');
        
        $file = $request->file('product_image');
        $destinationPath = 'asset/images/product';
        if($file){
            if($file->move($destinationPath, $file->getClientOriginalName())){
                $product_image = $destinationPath . '/' . $file->getClientOriginalName();
            }
            $product->product_image = $product_image;
        } else {
            $product->product_image = $destinationPath.'/no_product_image.png';
        }
        
        
        $check_product = DB::table('tbl_product')
                ->where('product_name', $product_name)
                ->count();
        if($check_product > 0){
            session::flash('message', error_message("Product name already exist"));
            return redirect()->action('ProductController@product_form');
        } else {
            $product->save();
            session::flash('message', success_message("Product name added successfully"));
            return redirect()->action('ProductController@product_form');
        }
    }
    
    public function delete_product($product_id){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $product = Product::find($product_id);
        DB::table('tbl_stock')
                ->where('product_id', $product_id)
                ->update(['product_id' => 1]);
        DB::table('tbl_sell')
                ->where('product_id', $product_id)
                ->update(['product_id' => 1]);
        $product_image = $product->product_image;
        if(isset($product_image)){
            @unlink($product_image);
        }
        $product->delete();
        session::flash('message', success_message("Product deleted successfully"));
        return redirect()->action('ProductController@product_form');
    }
    
    public function edit_product($product_id){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $product = Product::find($product_id);
        $title = "Update Product";
        return view('product.edit_product', ['product' => $product, 'title' => $title]);
    }
    
    public function update_product(Request $request){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $product_id = $request->product_id; 
        
        $product = Product::find($product_id);
        $product->product_name = $product_name = trim($request->product_name);
        $product->product_description = trim($request->product_description);
        
        $check_product = DB::table('tbl_product')
                ->where('product_name', $product_name)
                ->count();
        if($check_product > 0){
            session::flash('message', error_message("This product name already exist"));
            return redirect()->action('ProductController@edit_product', ['product_id' => $product_id]);
        } else {
            $product->save();
            session::flash('message', success_message("Product name updated successfully"));
            return redirect()->action('ProductController@product_form');
        }
    }
    
    public function update_product_image(Request $request){
        if(!$this->check_permission()){
            session::flash('message', error_message("Session expired, login required"));
            return redirect()->action('HomeController@index');
        }
        $product_id = $request->product_id;
        $product = Product::find($product_id);
        $product_image = $product->product_image;
        @unlink($product_image);
        $file = $request->file('product_image');
        $destinationPath = 'asset/images/product';
        if($file->move($destinationPath, $file->getClientOriginalName())){
            $product_image = $destinationPath . '/' . $file->getClientOriginalName();
        }
        $product->product_image = $product_image;
        $product->save();
        session::flash('message', success_message("Product name updated successfully"));
        return redirect()->action('ProductController@product_form');
    }
    
    
}
