<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{
    public $timestamps = false;
    protected  $table = 'tbl_buyer';
    protected $primaryKey = 'buyer_id';
    protected $fillable = ['buyer_name', 'buyer_description', 'buyer_phone', 'buyer_email', 
        'buyer_website', 'buyer_address', 'entry_time'];
}
