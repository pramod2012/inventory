/************************************
 * 
 * ##################################
 * # Javascript Custom Confirmation #
 * ##################################
 * 
 * Author     : Mohammad Kawser
 * Author URI : http://kawser.info 
 * 
 ************************************/
//var Confirm;
//$(document).ready(function(){
//    $(document).on("click", "#confirm", function(e){
//        
//    });
//});

var Confirm;
var delete_link;
$(document).ready(function () {
    
    function custormConfirm() {
        this.check = function (head, message) {
            delete_link = $("#delete").attr("href");
            var winW = window.innerWidth;
            var winH = window.innerHeight;
            var confirm_background = $("#confirm_background");
            var confirm_box = $("#confirm_box");

            confirm_background.css("display", "block");
            confirm_background.css("height", winH + "px");
            
            confirm_box.css("top", "100px");
            confirm_box.animate({"left": (winW/2) - (500/2) - 6 +"px"});
            confirm_box.fadeIn("slow");
            confirm_box.animate({"left": (winW/2) - (500/2) + 12 +"px"}, "fast");
            confirm_box.animate({"left": (winW/2) - (500/2) - 12 +"px"}, "fast");
            confirm_box.animate({"left": (winW/2) - (500/2)+"px"}, "fast");
            
            //confirm_box.css("display", "block");
            

            $("#confirm_box_head").html(head);
            $("#confirm_box_body").html(message);
            $("#confirm_box_footer").html(
                    "<button id='yes' onclick='Confirm.yes();'>Yes</button>" +
                    "<button id='no' onclick='Confirm.no();'>No</button>"
                    );
            
            
        }
        
        this.yes = function(){
            $("#confirm_box").css("display", "none");
            $("#confirm_background").css("display", "none");
            window.location = delete_link;
        }

        this.no = function(){
            $("#confirm_box").css("display", "none");
            $("#confirm_background").css("display", "none");
            return false;
        }

    }
    
    
    Confirm = new custormConfirm();
    
});

