-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 15, 2017 at 06:21 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `admin_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(40) NOT NULL,
  `admin_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `email`, `password`, `admin_name`) VALUES
(1, 'admin@localhost.com', 'bc53b5813c49642762c251319405523e399e6176', 'Head Admin');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_brand`
--

CREATE TABLE `tbl_brand` (
  `brand_id` int(11) NOT NULL,
  `brand_name` varchar(255) NOT NULL,
  `brand_description` text NOT NULL,
  `entry_time` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_brand`
--

INSERT INTO `tbl_brand` (`brand_id`, `brand_name`, `brand_description`, `entry_time`, `updated_at`) VALUES
(1, 'Deleted Brand', 'This brand was deleted', '2017-03-01 13:00:13', '2017-03-01 13:02:13'),
(2, 'Asus', 'Asus Brand Description', '2017-03-02 10:35:26', '2017-03-02 10:35:26'),
(3, 'HTC', 'HTC  Brand Description', '2017-03-02 10:35:56', '2017-03-02 10:35:56'),
(4, 'Samsung', 'Samsung Brand Description', '2017-03-02 10:36:36', '2017-03-02 10:36:36'),
(5, 'Nokia', 'Nokia Brand Description', '2017-03-02 10:36:51', '2017-03-02 10:36:51'),
(6, 'Lenovo', 'Lenovo Brand Description', '2017-03-02 10:37:06', '2017-03-02 10:37:06'),
(7, 'Apple', 'Apple Brand Description', '2017-03-02 10:37:16', '2017-03-02 10:37:16');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_buyer`
--

CREATE TABLE `tbl_buyer` (
  `buyer_id` int(11) NOT NULL,
  `buyer_name` varchar(255) NOT NULL,
  `buyer_description` text NOT NULL,
  `buyer_phone` varchar(100) NOT NULL,
  `buyer_email` varchar(255) NOT NULL,
  `buyer_website` text NOT NULL,
  `buyer_address` text NOT NULL,
  `entry_time` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_buyer`
--

INSERT INTO `tbl_buyer` (`buyer_id`, `buyer_name`, `buyer_description`, `buyer_phone`, `buyer_email`, `buyer_website`, `buyer_address`, `entry_time`, `updated_at`) VALUES
(1, 'Deleted buyer', '', '', '', '', '', '2017-03-01 13:10:39', '2017-03-01 13:10:39'),
(2, 'Raju', 'Raju is a regular buyer', '0168*******', 'raju@gmail.com', 'http://raju.com', 'Matlab, Chandpur.', '2017-03-03 03:59:23', '2017-03-03 03:59:23'),
(3, 'Arif', 'Arif is a buyer', '0168*******', 'arif@gmail.com', 'http://arif.com', 'Dhanmondi, Dhaka', '2017-03-03 04:00:20', '2017-03-03 04:00:20');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `entry_time` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`category_id`, `category_name`, `entry_time`, `updated_at`) VALUES
(1, 'Deleted Category', '2017-03-01 12:59:49', '2017-03-01 12:59:49'),
(2, 'Laptop', '2017-03-01 14:11:06', '2017-03-01 14:11:06'),
(3, 'Mobile & Tablet', '2017-03-02 10:15:54', '2017-03-02 10:25:55'),
(4, 'Watch', '2017-03-02 10:16:01', '2017-03-02 10:16:01'),
(5, 'Cameras & Photography', '2017-03-02 10:28:16', '2017-03-02 10:28:16');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_logo`
--

CREATE TABLE `tbl_logo` (
  `logo_id` int(11) NOT NULL,
  `logo` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_logo`
--

INSERT INTO `tbl_logo` (`logo_id`, `logo`) VALUES
(1, 'asset/images/logo/inventory-management-2.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_image` text NOT NULL,
  `product_description` text NOT NULL,
  `entry_time` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`product_id`, `product_name`, `product_image`, `product_description`, `entry_time`, `updated_at`) VALUES
(1, 'Deleted Product', 'asset/images/product/no_product_image.png', 'This product was deleted', '2017-03-01 13:09:17', '2017-03-01 13:09:17'),
(2, 'Lenovo Y50', 'asset/images/product/Lenovo-Y50-Touch-laptop-specs-review.jpg', 'Processor	4th Gen Intel® Core i7-4700HQ (2.40GHz 1600MHz 6MB)\r\nOperating System	Windows 8.1 64bit\r\nGraphics	NVIDIA GeForce GTX 860M (Maxwell) with up to 4GB Memory\r\nMemory	Up to 16GB PC3-12800 DDR3L SDRAM 1600 MHz\r\nDisplay	15.6-inch FHD (1920 x 1080) Display, AntiGlare (available in Touch and Non-Touch)\r\nOptical Drive	None (External only)\r\nDimensions	15.23" x 10.37" x 0.9" inches (W x D x H)\r\nWeight	5.29 lbs\r\nKeyboard	Backlit AccuType® keyboard\r\nCamera	720p HD Webcam\r\nStorage	Hybrid HDD up to 1TB 5400 RPM+8GB SSHD, or up to 512 SSD\r\nAudio	JBL® speakers with Dolby® Advanced Audio V2\r\nWireless	WiFi: Intel® Dual Band Wireless-AC 3160\r\nBluetooth® 4.0\r\nConnectors	2 x USB 3.0, 1 x USB 2.0, Audio Combo Jack (headphone and mic), HDMI-out, 4-in-1 (SD / MMC / SDXC / SDHC) card reader, RJ45, SPDIF\r\nBattery	4 Cell 54 Watt Hour Lithium-Ion\r\nUp to 5 hours WiFi browsing depending on configuration', '2017-03-02 10:51:05', '2017-03-02 10:51:05'),
(3, 'Lenovo Ideapad Y700', 'asset/images/product/lenovo-laptop-ideapad-y700-15-main.png', 'Processor	\r\nUp to 6th Generation Intel® Quad-Core i7 Processor\r\nOperating System	\r\nWindows 10 Home\r\nDiscrete Graphics	\r\nNVIDIA® GTX 960M 4 GB GDDR5\r\nMemory	\r\nUp to 16 GB DDR4\r\nWebcam	\r\nHD 720p, 2 x Internal Digital Array Microphone, or Intel 3D RealSense Camera (Optional)\r\nStorage	\r\nUp to 1 TB HDD / Up to 512 GB SSD / Up to 1 TB + 8 GB SSHD\r\nAudio	\r\n2 x 2W JBL Speakers with Chamber + 3.0 W Subwoofer and Dolby® Home Theater™\r\nBattery	\r\nUp to 4 Cell 60 WHr\r\nKeyboard	\r\nRed LED Backlit with 2 Level Brightness Control\r\nDisplay	\r\nFrameless Full HD (1920 x 1080) or Ultra HD (3840 x 2160) 16:9 IPS Anti-Glare\r\nDimensions	\r\n(inches) : 15.23" x 10.90" x 1.02"\r\n(mm) : 387 x 277 x 25.95\r\nWeight	\r\nStarting at 5.7 lbs (2.6 kg)\r\nODD	\r\nOptional External USB 2.0 DVD/BD Tray-In\r\nSecurity	\r\nKensington® Lock\r\nWLAN	\r\nLAN 1000 M, Up to WiFi 2 x 2 a/c + Bluetooth® 4.0\r\nPorts	\r\n2 x USB 3.0, 1 x USB 2.0 + Always-on, DC-in, 2-in-1 Audio Combo Jack, 1 x HDMI, 1 x 4-in-1 Media Card Reader (SD, SDHC, SDXC, MMC)', '2017-03-02 10:54:19', '2017-03-02 10:54:19'),
(4, 'iPhone 7', 'asset/images/product/iphone-7BLACK.png', 'NETWORK	Technology	\r\nGSM / CDMA / HSPA / EVDO / LTE\r\nLAUNCH	Announced	2016, September\r\nStatus	Available. Released 2016, September\r\nBODY	Dimensions	138.3 x 67.1 x 7.1 mm (5.44 x 2.64 x 0.28 in)\r\nWeight	138 g (4.87 oz)\r\nSIM	Nano-SIM\r\n 	- IP67 certified - dust and water resistant\r\n- Water resistant up to 1 meter and 30 minutes\r\n- Apple Pay (Visa, MasterCard, AMEX certified)\r\nDISPLAY	Type	LED-backlit IPS LCD, capacitive touchscreen, 16M colors\r\nSize	4.7 inches (~65.6% screen-to-body ratio)\r\nResolution	750 x 1334 pixels (~326 ppi pixel density)\r\nMultitouch	Yes\r\nProtection	Ion-strengthened glass, oleophobic coating\r\n 	- Wide color gamut display\r\n- 3D Touch display & home button\r\n- Display Zoom\r\nPLATFORM	OS	iOS 10.0.1, upgradable to iOS 10.2\r\nChipset	Apple A10 Fusion\r\nCPU	Quad-core 2.34 GHz (2x Hurricane + 2x Zephyr)\r\nGPU	PowerVR Series7XT Plus (six-core graphics)\r\nMEMORY	Card slot	No\r\nInternal	32/128/256 GB, GB, 2 GB RAM\r\nCAMERA	Primary	12 MP, f/1.8, 28mm, phase detection autofocus, OIS, quad-LED (dual tone) flash, check quality\r\nFeatures	1/3" sensor size, geo-tagging, simultaneous 4K video and 8MP image recording, touch focus, face/smile detection, HDR (photo/panorama)\r\nVideo	2160p@30fps, 1080p@30/60/120fps, 720p@240fps, check quality\r\nSecondary	7 MP, f/2.2, 32mm, 1080p@30fps, 720p@240fps, face detection, HDR, panorama\r\nSOUND	Alert types	Vibration, proprietary ringtones\r\nLoudspeaker	Yes, with stereo speakers\r\n3.5mm jack	No\r\n 	- Active noise cancellation with dedicated mic\r\n- Lightning to 3.5 mm headphone jack adapter incl.\r\nCOMMS	WLAN	Wi-Fi 802.11 a/b/g/n/ac, dual-band, hotspot\r\nBluetooth	v4.2, A2DP, LE\r\nGPS	Yes, with A-GPS, GLONASS\r\nNFC	Yes (Apple Pay only)\r\nRadio	No\r\nUSB	v2.0, reversible connector\r\nFEATURES	Sensors	Fingerprint (front-mounted), accelerometer, gyro, proximity, compass, barometer\r\nMessaging	iMessage, SMS (threaded view), MMS, Email, Push Email\r\nBrowser	HTML5 (Safari)\r\nJava	No\r\n 	- Siri natural language commands and dictation\r\n- iCloud cloud service\r\n- MP3/WAV/AAX+/AIFF/Apple Lossless player\r\n- MP4/H.264 player\r\n- Audio/video/photo editor\r\n- Document editor\r\nBATTERY	 	Non-removable Li-Ion 1960 mAh battery (7.45 Wh)\r\nTalk time	Up to 14 h (3G)\r\nMusic play	Up to 40 h\r\nMISC	Colors	Jet Black, Black, Silver, Gold, Rose Gold\r\nSAR US	1.19 W/kg (head)     1.19 W/kg (body)    \r\nSAR EU	1.38 W/kg (head)     1.34 W/kg (body)    \r\nPrice group	9/10\r\nTESTS	Performance	Basemark OS II 2.0: 3416\r\nDisplay	Contrast ratio: 1603:1 (nominal), 3.964 (sunlight)\r\nCamera	Photo / Video\r\nLoudspeaker	Voice 67dB / Noise 73dB / Ring 75dB\r\nAudio quality	Noise -92.4dB / Crosstalk -80.9dB\r\nBattery life	\r\nEndurance rating 61h', '2017-03-02 10:59:52', '2017-03-02 10:59:52'),
(5, 'iPhone 7 Plus', 'asset/images/product/iPhone7-matteBlack.png', 'NETWORK	Technology	\r\nGSM / CDMA / HSPA / EVDO / LTE\r\nLAUNCH	Announced	2016, September\r\nStatus	Available. Released 2016, September\r\nBODY	Dimensions	158.2 x 77.9 x 7.3 mm (6.23 x 3.07 x 0.29 in)\r\nWeight	188 g (6.63 oz)\r\nSIM	Nano-SIM\r\n 	- IP67 certified - dust and water resistant\r\n- Water resistant up to 1 meter and 30 minutes\r\n- Apple Pay (Visa, MasterCard, AMEX certified)\r\nDISPLAY	Type	LED-backlit IPS LCD, capacitive touchscreen, 16M colors\r\nSize	5.5 inches (~67.7% screen-to-body ratio)\r\nResolution	1080 x 1920 pixels (~401 ppi pixel density)\r\nMultitouch	Yes\r\nProtection	Ion-strengthened glass, oleophobic coating\r\n 	- Wide color gamut display\r\n- 3D Touch display & home button\r\n- Display Zoom\r\nPLATFORM	OS	iOS 10.0.1, upgradable to iOS 10.2\r\nChipset	Apple A10 Fusion\r\nCPU	Quad-core 2.34 GHz (2x Hurricane + 2x Zephyr)\r\nGPU	PowerVR Series7XT Plus (six-core graphics)\r\nMEMORY	Card slot	No\r\nInternal	32/128/256 GB, GB, 3 GB RAM\r\nCAMERA	Primary	Dual 12 MP, (28mm, f/1.8, OIS & 56mm, f/2.8), phase detection autofocus, 2x optical zoom, quad-LED (dual tone) flash, check quality\r\nFeatures	1/3" sensor size @ 28mm, 1/3.6" sensor size @ 56mm, geo-tagging, simultaneous 4K video and 8MP image recording, touch focus, face/smile detection, HDR (photo/panorama)\r\nVideo	2160p@30fps, 1080p@30/60/120fps, 720p@240fps, check quality\r\nSecondary	7 MP, f/2.2, 32mm, 1080p@30fps, 720p@240fps, face detection, HDR, panorama\r\nSOUND	Alert types	Vibration, proprietary ringtones\r\nLoudspeaker	Yes, with stereo speakers\r\n3.5mm jack	No\r\n 	- Active noise cancellation with dedicated mic\r\n- Lightning to 3.5 mm headphone jack adapter incl.\r\nCOMMS	WLAN	Wi-Fi 802.11 a/b/g/n/ac, dual-band, hotspot\r\nBluetooth	v4.2, A2DP, LE\r\nGPS	Yes, with A-GPS, GLONASS\r\nNFC	Yes (Apple Pay only)\r\nRadio	No\r\nUSB	v2.0, reversible connector\r\nFEATURES	Sensors	Fingerprint (front-mounted), accelerometer, gyro, proximity, compass, barometer\r\nMessaging	iMessage, SMS (threaded view), MMS, Email, Push Email\r\nBrowser	HTML5 (Safari)\r\nJava	No\r\n 	- Siri natural language commands and dictation\r\n- iCloud cloud service\r\n- MP3/WAV/AAX+/AIFF/Apple Lossless player\r\n- MP4/H.264 player\r\n- Audio/video/photo editor\r\n- Document editor\r\nBATTERY	 	Non-removable Li-Ion 2900 mAh battery (11.1 Wh)\r\nStand-by	Up to 384 h (3G)\r\nTalk time	Up to 21 h (3G)\r\nMusic play	Up to 60 h\r\nMISC	Colors	Jet Black, Black, Silver, Gold, Rose Gold\r\nSAR US	1.19 W/kg (head)     1.19 W/kg (body)    \r\nSAR EU	1.24 W/kg (head)     1.00 W/kg (body)    \r\nPrice group	9/10\r\nTESTS	Performance	Basemark OS II 2.0: 3796\r\nDisplay	Contrast ratio: 1398:1 (nominal), 3.588 (sunlight)\r\nCamera	Photo / Video\r\nLoudspeaker	Voice 68dB / Noise 72dB / Ring 72dB\r\nAudio quality	Noise -93.1dB / Crosstalk -80.5dB\r\nBattery life	\r\nEndurance rating 75h', '2017-03-02 11:01:55', '2017-03-04 13:27:58'),
(6, 'Nokia 3', 'asset/images/product/Nokia-3_Silver_White_Front-700px.jpg', 'Release date	February 2017\r\nForm factor	Touchscreen\r\nDimensions (mm)	143.40 x 71.40 x 8.48\r\nBattery capacity (mAh)	2650\r\nRemovable battery	No\r\nColours	Silver White, Matte Black, Tempered Blue, Copper White\r\nSAR value	NA\r\nDISPLAY\r\nScreen size (inches)	5.00\r\nTouchscreen	Yes\r\nResolution	720x1280 pixels\r\nHARDWARE\r\nProcessor	1.3GHz quad-core\r\nProcessor make	MediaTek 6737\r\nRAM	2GB\r\nInternal storage	16GB\r\nExpandable storage	Yes\r\nExpandable storage type	microSD\r\nExpandable storage up to (GB)	128\r\nCAMERA\r\nRear camera	8-megapixel\r\nFlash	Yes\r\nFront camera	8-megapixel\r\nSOFTWARE\r\nOperating System	Android 7.0\r\nCONNECTIVITY\r\nWi-Fi	Yes\r\nWi-Fi standards supported	802.11 a/b/g/n/ac\r\nGPS	Yes\r\nBluetooth	Yes\r\nNFC	No\r\nInfrared	No\r\nUSB OTG	Yes\r\nHeadphones	3.5mm\r\nFM	Yes\r\nNumber of SIMs	2\r\nSIM 1	\r\nSIM Type	Micro-SIM\r\nGSM/CDMA	GSM\r\n3G	Yes\r\n4G/ LTE	Yes\r\nSupports 4G in India (Band 40)	Yes\r\nSIM 2	\r\nSIM Type	Micro-SIM\r\nGSM/CDMA	GSM\r\n3G	Yes\r\n4G/ LTE	Yes\r\nSupports 4G in India (Band 40)	Yes\r\nSENSORS\r\nCompass/ Magnetometer	Yes\r\nProximity sensor	Yes\r\nAccelerometer	Yes\r\nAmbient light sensor	Yes\r\nGyroscope	Yes\r\nBarometer	No\r\nTemperature sensor	No', '2017-03-02 11:10:01', '2017-03-02 11:10:01'),
(7, 'Nokia 5', 'asset/images/product/Nokia-5_Tempered_Blue_Front.jpg', 'GENERAL\r\nRelease date	February 2017\r\nForm factor	Touchscreen\r\nDimensions (mm)	149.70 x 72.50 x 8.05\r\nBattery capacity (mAh)	3000\r\nRemovable battery	No\r\nColours	Tempered Blue, Copper, Matte Black, Silver\r\nSAR value	NA\r\nDISPLAY\r\nScreen size (inches)	5.20\r\nTouchscreen	Yes\r\nResolution	720x1280 pixels\r\nHARDWARE\r\nProcessor	octa-core\r\nProcessor make	Qualcomm Snapdragon 430\r\nRAM	2GB\r\nInternal storage	16GB\r\nExpandable storage	Yes\r\nExpandable storage type	microSD\r\nExpandable storage up to (GB)	128\r\nCAMERA\r\nRear camera	13-megapixel\r\nFlash	Yes\r\nFront camera	8-megapixel\r\nSOFTWARE\r\nOperating System	Android 7.1.1\r\nCONNECTIVITY\r\nWi-Fi	Yes\r\nWi-Fi standards supported	802.11 a/b/g/n/ac\r\nGPS	Yes\r\nBluetooth	Yes\r\nNFC	No\r\nInfrared	No\r\nUSB OTG	Yes\r\nHeadphones	3.5mm\r\nFM	No\r\nNumber of SIMs	2\r\nSIM 1	\r\nSIM Type	Nano-SIM\r\nGSM/CDMA	GSM\r\n3G	Yes\r\n4G/ LTE	Yes\r\nSupports 4G in India (Band 40)	Yes\r\nSIM 2	\r\nSIM Type	Nano-SIM\r\nGSM/CDMA	GSM\r\n3G	Yes\r\n4G/ LTE	Yes\r\nSupports 4G in India (Band 40)	Yes\r\nSENSORS\r\nCompass/ Magnetometer	Yes\r\nProximity sensor	Yes\r\nAccelerometer	Yes\r\nAmbient light sensor	Yes\r\nGyroscope	Yes\r\nBarometer	No\r\nTemperature sensor	No', '2017-03-02 11:11:35', '2017-03-02 11:11:35'),
(8, 'Nokia 6', 'asset/images/product/Nokia6-Matte-Black.jpg', '5.5-inch screen with 1,920x1,080-pixel resolution and Gorilla Glass\r\n16-megapixel rear camera\r\n 8-megapixel front-facing camera\r\nQualcomm Snapdragon 430 processor\r\n3GB RAM, 32GB internal storage (Nokia 6)\r\n4GB RAM, 64GB internal storage (Nokia 6 Arte Black Limited Edition)\r\nmicroSD card clot (up to 128GB)\r\nAluminum chassis\r\nAndroid 7.1.1 Nougat\r\n3,000mAh battery\r\nFingerprint sensor\r\nNFC', '2017-03-02 11:12:40', '2017-03-02 11:12:40');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sell`
--

CREATE TABLE `tbl_sell` (
  `sell_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `brand_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `sell_quantity` int(11) NOT NULL DEFAULT '0',
  `sell_rate` int(11) NOT NULL DEFAULT '0',
  `sell_total_price` int(11) NOT NULL DEFAULT '0',
  `sell_received_price` int(11) NOT NULL DEFAULT '0',
  `sell_due_price` int(11) NOT NULL DEFAULT '0',
  `receive_due` int(11) NOT NULL DEFAULT '0',
  `due_total` int(11) NOT NULL DEFAULT '0',
  `sell_date` date NOT NULL,
  `buyer_id` int(11) NOT NULL DEFAULT '0',
  `added_by` int(11) NOT NULL,
  `entry_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_sell`
--

INSERT INTO `tbl_sell` (`sell_id`, `category_id`, `brand_id`, `product_id`, `sell_quantity`, `sell_rate`, `sell_total_price`, `sell_received_price`, `sell_due_price`, `receive_due`, `due_total`, `sell_date`, `buyer_id`, `added_by`, `entry_time`) VALUES
(1, 3, 5, 6, 140, 22000, 3080000, 2500000, 580000, 0, 0, '2017-03-04', 2, 0, '2017-03-04 07:21:39'),
(2, 3, 5, 6, 200, 22000, 4400000, 4000000, 400000, 0, 0, '2017-03-04', 2, 0, '2017-03-04 07:33:14'),
(3, 0, 0, 0, 0, 0, 0, 0, 0, 900000, 980000, '2017-03-04', 2, 0, '2017-03-04 07:38:04');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_staff`
--

CREATE TABLE `tbl_staff` (
  `staff_id` int(11) NOT NULL,
  `staff_name` varchar(100) NOT NULL,
  `staff_phone` varchar(100) NOT NULL,
  `staff_email` varchar(255) NOT NULL,
  `staff_password` varchar(40) NOT NULL,
  `staff_address` text NOT NULL,
  `category_perm` int(1) NOT NULL DEFAULT '0',
  `brand_perm` int(1) NOT NULL DEFAULT '0',
  `product_perm` int(1) NOT NULL DEFAULT '0',
  `supplier_perm` int(1) NOT NULL DEFAULT '0',
  `buyer_perm` int(1) NOT NULL DEFAULT '0',
  `stock_perm` int(1) NOT NULL DEFAULT '0',
  `sell_perm` int(1) NOT NULL DEFAULT '0',
  `entry_time` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_staff`
--

INSERT INTO `tbl_staff` (`staff_id`, `staff_name`, `staff_phone`, `staff_email`, `staff_password`, `staff_address`, `category_perm`, `brand_perm`, `product_perm`, `supplier_perm`, `buyer_perm`, `stock_perm`, `sell_perm`, `entry_time`, `updated_at`) VALUES
(1, 'Staff One', '01918888888', 'staffmail@mail.com', 'bc53b5813c49642762c251319405523e399e6176', 'this is staff address 1234', 0, 0, 0, 0, 0, 0, 0, '2017-01-21 11:56:16', '2017-03-04 13:39:41');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_stock`
--

CREATE TABLE `tbl_stock` (
  `stock_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `brand_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `stock_quantity` int(11) NOT NULL DEFAULT '0',
  `stock_rate` int(11) NOT NULL DEFAULT '0',
  `total_price` int(11) NOT NULL DEFAULT '0',
  `stock_paid_amount` int(11) NOT NULL DEFAULT '0',
  `stock_due_amount` int(11) NOT NULL DEFAULT '0',
  `pay_due` int(11) NOT NULL DEFAULT '0',
  `due_total` int(11) NOT NULL DEFAULT '0',
  `stock_date` date NOT NULL,
  `supplier_id` int(11) NOT NULL DEFAULT '0',
  `added_by` int(11) NOT NULL DEFAULT '0',
  `entry_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `quantity_remains` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_stock`
--

INSERT INTO `tbl_stock` (`stock_id`, `category_id`, `brand_id`, `product_id`, `stock_quantity`, `stock_rate`, `total_price`, `stock_paid_amount`, `stock_due_amount`, `pay_due`, `due_total`, `stock_date`, `supplier_id`, `added_by`, `entry_time`, `quantity_remains`) VALUES
(1, 3, 5, 6, 120, 20000, 2400000, 2000000, 400000, 0, 0, '2017-03-03', 3, 0, '2017-03-04 13:31:15', 0),
(2, 3, 5, 6, 180, 21000, 3780000, 3000000, 780000, 0, 0, '2017-03-04', 3, 0, '2017-03-04 13:32:04', 100),
(3, 0, 0, 0, 0, 0, 0, 0, 0, 1000000, 1180000, '2017-03-04', 3, 0, '2017-03-04 13:36:34', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_supplier`
--

CREATE TABLE `tbl_supplier` (
  `supplier_id` int(11) NOT NULL,
  `supplier_name` varchar(255) NOT NULL,
  `supplier_description` text NOT NULL,
  `supplier_phone` varchar(100) NOT NULL,
  `supplier_email` varchar(255) NOT NULL,
  `supplier_website` text NOT NULL,
  `supplier_address` text NOT NULL,
  `entry_time` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_supplier`
--

INSERT INTO `tbl_supplier` (`supplier_id`, `supplier_name`, `supplier_description`, `supplier_phone`, `supplier_email`, `supplier_website`, `supplier_address`, `entry_time`, `updated_at`) VALUES
(1, 'Deleted Supplier', '', '', '', '', '', '2017-03-01 13:10:24', '2017-03-01 13:10:24'),
(2, 'Reza Khan', 'Reza khan is a good supplier', '0191*******', 'reza@gmail.com', 'http://rezakhan.com', 'Ajimpur, Dhaka.', '2017-03-03 03:54:48', '2017-03-03 03:54:48'),
(3, 'Nazmul Hasan', 'Nazmul Hasan is a good and regular supplier', '0191*******', 'nazmulhasan@gmail.com', 'nazmulhasan.com', 'Mirpur, Dhaka.', '2017-03-03 03:56:19', '2017-03-03 03:56:19');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_title`
--

CREATE TABLE `tbl_title` (
  `title_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_title`
--

INSERT INTO `tbl_title` (`title_id`, `title`) VALUES
(1, 'Stock Book');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_brand`
--
ALTER TABLE `tbl_brand`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `tbl_buyer`
--
ALTER TABLE `tbl_buyer`
  ADD PRIMARY KEY (`buyer_id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `tbl_logo`
--
ALTER TABLE `tbl_logo`
  ADD PRIMARY KEY (`logo_id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `tbl_sell`
--
ALTER TABLE `tbl_sell`
  ADD PRIMARY KEY (`sell_id`);

--
-- Indexes for table `tbl_staff`
--
ALTER TABLE `tbl_staff`
  ADD PRIMARY KEY (`staff_id`);

--
-- Indexes for table `tbl_stock`
--
ALTER TABLE `tbl_stock`
  ADD PRIMARY KEY (`stock_id`);

--
-- Indexes for table `tbl_supplier`
--
ALTER TABLE `tbl_supplier`
  ADD PRIMARY KEY (`supplier_id`);

--
-- Indexes for table `tbl_title`
--
ALTER TABLE `tbl_title`
  ADD PRIMARY KEY (`title_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_brand`
--
ALTER TABLE `tbl_brand`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_buyer`
--
ALTER TABLE `tbl_buyer`
  MODIFY `buyer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_logo`
--
ALTER TABLE `tbl_logo`
  MODIFY `logo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_sell`
--
ALTER TABLE `tbl_sell`
  MODIFY `sell_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_staff`
--
ALTER TABLE `tbl_staff`
  MODIFY `staff_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_stock`
--
ALTER TABLE `tbl_stock`
  MODIFY `stock_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_supplier`
--
ALTER TABLE `tbl_supplier`
  MODIFY `supplier_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_title`
--
ALTER TABLE `tbl_title`
  MODIFY `title_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
