<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'HomeController@index');
Route::post('login', 'Homecontroller@login');
Route::get('logout', 'HomeController@logout');
Route::get('access_denied', 'HomeController@access_denied');

Route::get('home', 'HomeController@home');

//Config
Route::get('config', 'ConfigController@config_home');
Route::get('title', 'TitleController@title');
Route::post('title', 'TitleController@save_title');
Route::put('title', 'TitleController@update_title');
Route::get('logo', 'LogoController@logo');
Route::post('logo', 'LogoController@save_logo');
Route::put('logo', 'LogoController@update_logo');

//Category
Route::get('add_category', 'CategoryController@category_form');
Route::post('add_category', 'CategoryController@add_category');
Route::get('delete_category/{category_id}', 'CategoryController@delete_category');
Route::get('edit_category/{category_id}', 'CategoryController@edit_category');
Route::put('update_category', 'CategoryController@update_category');
Route::get('category_history_all/{category_id}', 'CategoryController@search_category');

Route::get('add_brand', 'BrandController@brand_form');
Route::post('add_brand', 'BrandController@add_brand');
Route::get('delete_brand/{brand_id}', 'BrandController@delete_brand');
Route::get('edit_brand/{brand_id}', 'BrandController@edit_brand');
Route::put('update_brand', 'BrandController@update_brand');
Route::get('brand_history_all/{brand_id}', 'BrandController@search_brand');

Route::get('add_product', 'ProductController@product_form');
Route::post('add_product', 'ProductController@add_product');
Route::get('delete_product/{product_id}', 'ProductController@delete_product');
Route::get('edit_product/{product_id}', 'ProductController@edit_product');
Route::put('update_product', 'ProductController@update_product');
Route::put('update_product_image', 'ProductController@update_product_image');
Route::get('product_history_all/{product_id}', 'ProductController@search_product');

Route::get('add_supplier', 'SupplierController@supplier_form');
Route::post('add_supplier', 'SupplierController@add_supplier');
Route::get('delete_supplier/{supplier_id}', 'SupplierController@delete_supplier');
Route::get('edit_supplier/{supplier_id}', 'SupplierController@edit_supplier');
Route::put('update_supplier', 'SupplierController@update_supplier');
Route::get('view_supplier/{supplier_id}', 'SupplierController@view_supplier');

Route::get('add_buyer', 'BuyerController@buyer_form');
Route::post('add_buyer', 'BuyerController@add_buyer');
Route::get('delete_buyer/{buyer_id}', 'BuyerController@delete_buyer');
Route::get('edit_buyer/{buyer_id}', 'BuyerController@edit_buyer');
Route::put('update_buyer', 'BuyerController@update_buyer');
Route::get('view_buyer/{buyer_id}', 'BuyerController@view_buyer');

Route::get('add_staff', 'StaffController@staff_form');
Route::post('add_staff', 'StaffController@add_staff');
Route::get('delete_staff/{staff_id}', 'StaffController@delete_staff');
Route::get('edit_staff/{staff_id}', 'StaffController@edit_staff');
Route::put('update_staff', 'StaffController@update_staff');
Route::get('view_staff/{staff_id}', 'StaffController@view_staff');

Route::get('updateActivePerm/{staff_id}/{perm}', 'StaffController@updateActivePerm');
Route::get('updateInactivePerm/{staff_id}/{perm}', 'StaffController@updateInactivePerm');

//Stock 
Route::get('pay_due/{supplier_id}', 'StockController@due_pay_form');
Route::post('save_pay', 'StockController@save_due');
Route::get('stock', 'StockController@stock_form');
Route::post('stock', 'StockController@save_stock');

Route::get('stock_history', 'StockController@stock_history');
Route::get('available_stock', 'StockController@available_stock');

Route::get('check_stock/{category_id}/{brand_id}/{product_id}', 'StockController@check_stocks');

//sell
Route::get('receive_due/{buyer_id}', 'SellController@due_receive_form');
Route::post('save_due', 'SellController@save_due');
Route::get('sell', 'SellController@sell_form');
Route::post('sell', 'SellController@save_sell');
Route::get('sell_history', 'SellController@sell_history');


//Build search
Route::post('search', 'SearchController@search');


//History
Route::get('buyer_history_all/{buyer_id}', 'StateController@buyer_history_all');
Route::post('buyer_history', 'StateController@buyer_history'); //daet wise history

Route::get('supplier_history_all/{supplier_id}', 'StateController@supplier_history_all');
Route::post('supplier_history', 'StateController@supplier_history'); //daet wise history

//settings
Route::get('settings', 'SettingsController@index');
Route::post('update_password', 'SettingsController@update_password');