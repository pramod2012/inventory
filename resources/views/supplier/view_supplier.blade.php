@extends('home')

@section('maincontent')

<div id="view_page">
    <h3 class="title_three">Supplier Information</h3>
    <div class="view_page_padding">
    <p><span>Supplier Name</span>: <?php echo $supplier->supplier_name; ?></p>
    
    <h4 class="title_four">About Supplier</h4>
    <article><?php echo $supplier->supplier_description; ?></article>
    </div>
    <div class="view_table">
        <table>
            <tr>
                <th>Phone</th>
                <td><?php echo $supplier->supplier_phone; ?></td>
            </tr>
            <tr>
                <th>Email</th>
                <td><?php echo $supplier->supplier_email; ?></td>
            </tr>
            <tr>
                <th>Website</th>
                <td><?php echo $supplier->supplier_website; ?></td>
            </tr>
            <tr>
                <th>Address</th>
                <td><?php echo $supplier->supplier_address; ?></td>
            </tr>
        </table>
    </div>
    
    <div class="footer_link">
        <a href="<?php echo url('/'); ?>">Home</a>
        <a href="<?php echo url('/'); ?>/add_supplier">Supplier</a>
    </div>
    
</div>



@endsection