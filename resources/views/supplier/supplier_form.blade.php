@extends('home')

@section('maincontent')
@include('scripts.supplier_search')
<h2 class="title_two"><?php echo $title; ?></h2>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div class="simple_form">
            <?php 
            if(Session::has('message')){
                echo Session::get('message');
            }
            ?>
            <form action="<?php echo url('/'); ?>/add_supplier" method="post">
                <label for="bn">Supplier Name</label>
                <input type="text" name="supplier_name" id="bn" placeholder="Supplier Name">
                <label for="bd">Supplier Description</label>
                <textarea name="supplier_description" placeholder="Supplier Description"></textarea>
                <label for="bn">Supplier Phone</label>
                <input type="text" name="supplier_phone" id="bn" placeholder="Supplier Phone">
                <label for="bn">Supplier Email</label>
                <input type="email" name="supplier_email" id="bn" placeholder="Supplier Email">
                <label for="bn">Supplier Website</label>
                <input type="text" name="supplier_website" id="bn" placeholder="Supplier Website">
                <label for="bd">Supplier Address</label>
                <textarea name="supplier_address" placeholder="Supplier Address"></textarea>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
                <input type="submit" value="Add">
            </form>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div class="search_form">
            <h4 class="title_four">Search Supplier Statistics</h4>
            <form action="<?php echo url('/'); ?>/supplier_history" method="post">
                <label for="supplier">Supplier</label>
                <input type="text" id="supplier" placeholder="Supplier" autocomplete="off">
                <span id="search_result">
                    
                </span>
                <input type="hidden" name="supplier_id" value="" id="supplier_id"/>
                <div id="bdata_box">

                    <?php foreach ($all_suppliers as $b) { ?>
                        <option value="<?php echo $b->supplier_id; ?>"><?php echo $b->supplier_name; ?></option>
                    <?php } ?>

                </div>
                <div>
                    <div class="form_element_side">
                        <label for="from_date">From Date</label>
                        <input type="date" id="from_date" name="from_date" placeholder="From Date" autocomplete="off">
                    </div>
                    <div class="form_element_side">
                        <label for="to_date">To Date</label>
                        <input type="date" id="to_date" name="to_date" placeholder="To Date" autocomplete="off">
                    </div>
                    <div style="clear: both"></div>
                </div>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
                <input type="submit" value="Search" >
            </form>
        </div>
        <div class="basic_table">
            <table>
                <tr>
                    <th>Supplier Name</th>
                    <th>Action</th>
                </tr>
                <?php foreach($suppliers as $s) { ?>
                <tr>
                    <td><?php echo $s->supplier_name; ?></td>
                    <td>
                        <a id="view" href="<?php echo url('/'); ?>/view_supplier/<?php echo $s->supplier_id; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                        <a id="edit" href="<?php echo url('/'); ?>/edit_supplier/<?php echo $s->supplier_id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <a id="delete" href="<?php  echo url('/'); ?>/delete_supplier/<?php echo $s->supplier_id; ?>" onclick="Confirm.check('Confirm !', 'Are you sure to delete this?'); return false;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        
                    </td>
                </tr>
                <?php } ?>
            </table>
            <div class="page_links">
                <?php echo $suppliers->links(); ?>
            </div>
        </div>
    </div>
</div>





@endsection