@extends('home')

@section('maincontent')
<h2 class="title_two">Edit Supplier</h2>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div class="simple_form">
            <?php 
            if(Session::has('message')){
                echo Session::get('message');
            }
            ?>
            <form action="<?php echo url('/'); ?>/update_supplier" method="post">
                <input type="hidden" name="supplier_id" value="<?php echo $supplier->supplier_id; ?>" >
                <label for="bn">Supplier Name</label>
                <input type="text" name="supplier_name" id="bn" value="<?php echo $supplier->supplier_name; ?>">
                <label for="bd">Supplier Description</label>
                <textarea name="supplier_description"><?php echo $supplier->supplier_description; ?></textarea>
                <label for="bn">Supplier Phone</label>
                <input type="text" name="supplier_phone" id="bn" value="<?php echo $supplier->supplier_phone; ?>">
                <label for="bn">Supplier Email</label>
                <input type="email" name="supplier_email" id="bn" value="<?php echo $supplier->supplier_email; ?>">
                <label for="bn">Supplier Website</label>
                <input type="text" name="supplier_website" id="bn" value="<?php echo $supplier->supplier_website; ?>">
                <label for="bd">Supplier Address</label>
                <textarea name="supplier_address"><?php echo $supplier->supplier_address; ?></textarea>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
                <input type="hidden" name="_method" value="PUT" >
                <input type="submit" value="Update">
            </form>
        </div>
    </div>
    
</div>


@endsection