@extends('home')

@section('maincontent')
@include('scripts.buyer_search')
<h2 class="title_two"><?php echo $title; ?></h2>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div class="simple_form">
            <?php
            if (Session::has('message')) {
                echo Session::get('message');
            }
            ?>
            <form action="<?php echo url('/'); ?>/add_buyer" method="post">
                <label for="bn">Buyer Name</label>
                <input type="text" name="buyer_name" id="bn" placeholder="Buyer Name">
                <label for="bd">Buyer Description</label>
                <textarea name="buyer_description" placeholder="Buyer Description"></textarea>
                <label for="bn">Buyer Phone</label>
                <input type="text" name="buyer_phone" id="bn" placeholder="Buyer Phone">
                <label for="bn">Buyer Email</label>
                <input type="email" name="buyer_email" id="bn" placeholder="Buyer Email">
                <label for="bn">Buyer Website</label>
                <input type="text" name="buyer_website" id="bn" placeholder="Buyer Website">
                <label for="bd">Buyer Address</label>
                <textarea name="buyer_address" placeholder="Buyer Address"></textarea>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
                <input type="submit" value="Add">
            </form>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        
        <div class="search_form">
            <?php
                if (Session::has('m')) {
                    echo Session::get('m');
                }
            ?>
            <h4 class="title_four">Search Buyer Statistics</h4>
            <form action="<?php echo url('/'); ?>/buyer_history" method="post">
                <label for="buyer">Buyer</label>
                <input type="text" id="buyer" placeholder="Buyer" autocomplete="off">
                <span id="search_result">
                    
                </span>
                <input type="hidden" name="buyer_id" value="" id="buyer_id"/>
                <div id="bdata_box">

                    <?php foreach ($all_buyers as $b) { ?>
                        <option value="<?php echo $b->buyer_id; ?>"><?php echo $b->buyer_name; ?></option>
                    <?php } ?>

                </div>
                <div>
                    <div class="form_element_side">
                        <label for="from_date">From Date</label>
                        <input type="date" id="from_date" name="from_date" placeholder="From Date" autocomplete="off">
                    </div>
                    <div class="form_element_side">
                        <label for="to_date">To Date</label>
                        <input type="date" id="to_date" name="to_date" placeholder="To Date" autocomplete="off">
                    </div>
                    <div style="clear: both"></div>
                </div>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
                <input type="submit" value="Search" >
            </form>
        </div>
        <div class="basic_table">
            <table>
                <tr>
                    <th>Buyer Name</th>
                    <th>Action</th>
                </tr>
                <?php foreach ($buyers as $s) { ?>
                    <tr>
                        <td><?php echo $s->buyer_name; ?></td>
                        <td>
                            <a id="view" href="<?php echo url('/'); ?>/view_buyer/<?php echo $s->buyer_id; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            <a id="edit" href="<?php echo url('/'); ?>/edit_buyer/<?php echo $s->buyer_id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            <a id="delete" href="<?php echo url('/'); ?>/delete_buyer/<?php echo $s->buyer_id; ?>" onclick="Confirm.check('Confirm !', 'Are you sure to delete this?');
                                    return false;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>

                        </td>
                    </tr>
<?php } ?>
            </table>
            <div class="page_links">
<?php echo $buyers->links(); ?>
            </div>
        </div>
    </div>
</div>





@endsection