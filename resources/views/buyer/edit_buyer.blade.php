@extends('home')

@section('maincontent')
<h2 class="title_two">Edit Buyer</h2>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div class="simple_form">
            <?php 
            if(Session::has('message')){
                echo Session::get('message');
            }
            ?>
            <form action="<?php echo url('/'); ?>/update_buyer" method="post">
                <input type="hidden" name="buyer_id" value="<?php echo $buyer->buyer_id; ?>" >
                <label for="bn">Buyer Name</label>
                <input type="text" name="buyer_name" id="bn" value="<?php echo $buyer->buyer_name; ?>">
                <label for="bd">Buyer Description</label>
                <textarea name="buyer_description"><?php echo $buyer->buyer_description; ?></textarea>
                <label for="bn">Buyer Phone</label>
                <input type="text" name="buyer_phone" id="bn" value="<?php echo $buyer->buyer_phone; ?>">
                <label for="bn">Buyer Email</label>
                <input type="email" name="buyer_email" id="bn" value="<?php echo $buyer->buyer_email; ?>">
                <label for="bn">Buyer Website</label>
                <input type="text" name="buyer_website" id="bn" value="<?php echo $buyer->buyer_website; ?>">
                <label for="bd">Buyer Address</label>
                <textarea name="buyer_address"><?php echo $buyer->buyer_address; ?></textarea>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
                <input type="hidden" name="_method" value="PUT" >
                <input type="submit" value="Update">
            </form>
        </div>
    </div>
    
</div>


@endsection