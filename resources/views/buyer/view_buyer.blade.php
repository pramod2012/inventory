@extends('home')

@section('maincontent')
<div id="view_page">
    <h3 class="title_three">Buyer Information</h3>
    <div class="view_page_padding">
    <p><span>Buyer Name</span>: <?php echo $buyer->buyer_name; ?></p>
    
    <h4 class="title_four">About Buyer</h4>
    <article><?php echo $buyer->buyer_description; ?></article>
    </div>
    <div class="view_table">
        <table>
            <tr>
                <th>Phone</th>
                <td><?php echo $buyer->buyer_phone; ?></td>
            </tr>
            <tr>
                <th>Email</th>
                <td><?php echo $buyer->buyer_email; ?></td>
            </tr>
            <tr>
                <th>Website</th>
                <td><?php echo $buyer->buyer_website; ?></td>
            </tr>
            <tr>
                <th>Address</th>
                <td><?php echo $buyer->buyer_address; ?></td>
            </tr>
        </table>
    </div>
    
    <div class="footer_link">
        <a href="<?php echo url('/'); ?>">Home</a>
        <a href="<?php echo url('/'); ?>/add_buyer">Buyer</a>
    </div>
    
</div>



@endsection