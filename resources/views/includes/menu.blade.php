<nav>
    <ul>
        <li><a href="<?php echo url('/'); ?>">Home</a></li>
        <li><a href="<?php echo url('/'); ?>/add_category">Category</a></li>
        <li><a href="<?php echo url('/'); ?>/add_brand">Brands</a></li>
        <li><a href="<?php echo url('/'); ?>/add_product">Products</a></li>
        <li><a href="<?php echo url('/'); ?>/add_supplier">Supplier</a></li>
        <li><a href="<?php echo url('/'); ?>/add_buyer">Buyer</a></li>
        <li><span></span></li>
        <div style="clear: both;"></div>
    </ul>
</nav>
<div id="show_menu">Menu</div>
<div id="close_menu">Close</div>