@extends('home')

@section('maincontent')
<h2 class="title_two">Edit Staff</h2>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div class="simple_form">
            <?php 
            if(Session::has('message')){
                echo Session::get('message');
            }
            ?>
            <form action="<?php echo url('/'); ?>/update_staff" method="post">
                <input type="hidden" name="staff_id" value="<?php echo $staff->staff_id; ?>" >
                <label for="bn">Staff Name</label>
                <input type="text" name="staff_name" id="bn" value="<?php echo $staff->staff_name; ?>">
                <label for="bn">Staff Phone</label>
                <input type="text" name="staff_phone" id="bn" value="<?php echo $staff->staff_phone; ?>">
                <label for="bn">Staff Email</label>
                <input type="email" name="staff_email" id="bn" value="<?php echo $staff->staff_email; ?>">
                <label for="bn">Staff Password</label>
                <input type="password" name="staff_password" id="bn" value="<?php echo $staff->staff_password; ?>">
                <label for="bd">Staff Address</label>
                <textarea name="staff_address"><?php echo $staff->staff_address; ?></textarea>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
                <input type="hidden" name="_method" value="PUT" >
                <input type="submit" value="Update">
            </form>
        </div>
    </div>
    
</div>


@endsection