@extends('home')

@section('maincontent')
<h2 class="title_two"><?php echo $title; ?></h2>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div class="simple_form">
            <?php 
            if(Session::has('message')){
                echo Session::get('message');
            }
            ?>
            <form action="<?php echo url('/'); ?>/add_staff" method="post">
                <label for="bn">Staff Name</label>
                <input type="text" name="staff_name" id="bn" placeholder="Staff Name">
                <label for="bn">Staff Phone</label>
                <input type="text" name="staff_phone" id="bn" placeholder="Staff Phone">
                <label for="bn">Staff Email</label>
                <input type="email" name="staff_email" id="bn" placeholder="Staff Email">
                <label for="bn">Staff Password</label>
                <input type="password" name="staff_password" id="bn" placeholder="Staff Password">
                <label for="bd">Staff Address</label>
                <textarea name="staff_address" placeholder="Staff Address"></textarea>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
                <input type="submit" value="Add">
            </form>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div class="basic_table">
            <table>
                <tr>
                    <th>Staff Name</th>
                    <th>Action</th>
                </tr>
                <?php foreach($staffs as $s) { ?>
                <tr>
                    <td><?php echo $s->staff_name; ?></td>
                    <td>
                        <a id="view" href="<?php echo url('/'); ?>/view_staff/<?php echo $s->staff_id; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                        <a id="edit" href="<?php echo url('/'); ?>/edit_staff/<?php echo $s->staff_id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <a id="delete" href="<?php  echo url('/'); ?>/delete_staff/<?php echo $s->staff_id; ?>" onclick="Confirm.check('Confirm !', 'Are you sure to delete this?'); return false;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        
                    </td>
                </tr>
                <?php } ?>
            </table>
            <div class="page_links">
                <?php echo $staffs->links(); ?>
            </div>
        </div>
    </div>
</div>





@endsection