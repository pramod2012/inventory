@extends('home')

@section('maincontent')

<div id="view_page">
    <h3 class="title_three">Staff Information</h3>
    <div class="view_page_padding">
        <p><span>Staff Name</span>: <?php echo $staff->staff_name; ?></p>

    </div>

    <script>
        var a;
        var b;
        
        function doOn(e){
            $(e.target).attr("id", "permission_area_on");
            a = 1;
        }
        
        function doOff(e){
            $(e.target).attr("id", "permission_area");
            b = 1;
        }
        
        $(document).ready(function () {

            $(document).on("click", "#permission_area", function (e) {

                staff_id = $(this).attr("staff_id");
                perm = $(this).attr("perm");

                $.ajax({
                    type: "GET",
                    url: "<?php echo url('/'); ?>/updateActivePerm/" + staff_id + "/" + perm,
                    success: function (msg) {
                        if (msg == "true") {
                            a = 2;
                            doOn(e);
                        } else {
                            alert("Something went wrong");
                        }

                    }

                });
                /**/

            });

            $(document).on("click", "#permission_area_on", function (e) {

                staff_id = $(this).attr("staff_id");
                perm = $(this).attr("perm");

                $.ajax({
                    type: "GET",
                    url: "<?php echo url('/'); ?>/updateInactivePerm/" + staff_id + "/" + perm,
                    success: function (msg) {
                        if (msg == "true") {
                            b = 2;
                            doOff(e);
                        } else {
                            alert("Something went wrong");
                        }
                    }
                });
                /**/
                
            });



        });


    </script>
    <div class="permission_table">
        <table>
            <tr>
                <th colspan="2">Permission</th>
            </tr>
            <tr>
                <td>Category</td>
                <td><?php
                    $c_perm = $staff->category_perm;
                    if ($c_perm == 1) {
                        $cid = "permission_area_on";
                    } else if ($c_perm == 0) {
                        $cid = "permission_area";
                    }
                    ?>
                    <div id="<?php echo $cid; ?>" staff_id="<?php echo $staff->staff_id; ?>" perm="category_perm"></div>
                    <div id="permission_switch"></div>
                </td>
            </tr>
            <tr>
                <td>Brand</td>
                <td>
                    <?php
                    $b_perm = $staff->brand_perm;
                    if ($b_perm == 1) {
                        $bid = "permission_area_on";
                    } else if ($b_perm == 0) {
                        $bid = "permission_area";
                    }
                    ?>
                    <div id="<?php echo $bid; ?>" staff_id="<?php echo $staff->staff_id; ?>" perm="brand_perm"></div>
                    <div id="permission_switch"></div>
                </td>
            </tr>
            
            <tr>
                <td>Product</td>
                <td><?php
                    $p_perm = $staff->product_perm;
                    if ($p_perm == 1) {
                        $pid = "permission_area_on";
                    } else if ($p_perm == 0) {
                        $pid = "permission_area";
                    }
                    ?>
                    <div id="<?php echo $pid; ?>" staff_id="<?php echo $staff->staff_id; ?>" perm="product_perm"></div>
                    <div id="permission_switch"></div>
                </td>
            </tr>
            
            <tr>
                <td>Supplier</td>
                <td><?php
                    $s_perm = $staff->supplier_perm;
                    if ($s_perm == 1) {
                        $sid = "permission_area_on";
                    } else if ($s_perm == 0) {
                        $sid = "permission_area";
                    }
                    ?>
                    <div id="<?php echo $sid; ?>" staff_id="<?php echo $staff->staff_id; ?>" perm="supplier_perm"></div>
                    <div id="permission_switch"></div>
                </td>
            </tr>
            <tr>
                <td>Buyer</td>
                <td><?php
                    $buy_perm = $staff->buyer_perm;
                    if ($buy_perm == 1) {
                        $buyid = "permission_area_on";
                    } else if ($buy_perm == 0) {
                        $buyid = "permission_area";
                    }
                    ?>
                    <div id="<?php echo $buyid; ?>" staff_id="<?php echo $staff->staff_id; ?>" perm="buyer_perm"></div>
                    <div id="permission_switch"></div>
                </td>
            </tr>
            <tr>
                <td>Stock</td>
                <td><?php
                    $stock_perm = $staff->stock_perm;
                    if ($stock_perm == 1) {
                        $stockid = "permission_area_on";
                    } else if ($stock_perm == 0) {
                        $stockid = "permission_area";
                    }
                    ?>
                    <div id="<?php echo $stockid; ?>" staff_id="<?php echo $staff->staff_id; ?>" perm="stock_perm"></div>
                    <div id="permission_switch"></div>
                </td>
            </tr>
            <tr>
                <td>Sell</td>
                <td><?php
                    $sell_perm = $staff->sell_perm;
                    if ($sell_perm == 1) {
                        $sellid = "permission_area_on";
                    } else if ($sell_perm == 0) {
                        $sellid = "permission_area";
                    }
                    ?>
                    <div id="<?php echo $sellid; ?>" staff_id="<?php echo $staff->staff_id; ?>" perm="sell_perm"></div>
                    <div id="permission_switch"></div>
                </td>
            </tr>
        </table>
    </div>

    <div class="view_table">
        <table>
            <tr>
                <th>Phone</th>
                <td><?php echo $staff->staff_phone; ?></td>
            </tr>
            <tr>
                <th>Email</th>
                <td><?php echo $staff->staff_email; ?></td>
            </tr>
            <tr>
                <th>Address</th>
                <td><?php echo $staff->staff_address; ?></td>
            </tr>
        </table>
    </div>

    <div class="footer_link">
        <a href="<?php echo url('/'); ?>">Home</a>
        <a href="<?php echo url('/'); ?>/add_staff">Staff</a>
    </div>

</div>



@endsection