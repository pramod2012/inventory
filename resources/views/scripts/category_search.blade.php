<script>
            var category_matched_result;
            var category_total_result;

            var category_matched_line;
            var category_matched_len;
            var category_input_len;


            $(document).ready(function () {

                $("#category").keyup(function () {
                    var category_name = $(this).val();
                    var cn = toTitleCase(category_name);
                    var cn_len = cn.length;

                    $("#bdata_box option").addClass("bdata_result");
                    $("#search_result").html("");

                    if (cn_len > 0) {

                        if (jQuery.trim(cn) != '') {

                            var category_matched_result = $("#bdata_box option:contains('" + cn + "')").removeClass("bdata_result");
                            $("#bdata_box").css("display", "inline-block");
                            $("#bdata_box").fadeIn("slow");
                            var category_total_result = category_matched_result.length;

                            if (category_total_result == 1) {
                                category_matched_line = category_matched_result.html();
                                category_matched_len = category_matched_line.length;
                                category_input_len = cn_len;
                                if (category_input_len === category_matched_len) {
                                    var category_id = category_matched_result.val();
                                    var category_name = category_matched_line;
                                    $("#category").val(category_name);
                                    $("#category_id").val(category_id);
                                    $("#search_result").html('<a href="<?php echo url('/'); ?>/category_history_all/'+category_id+'">'+category_name+'</a>');
                                    $("#bdata_box").fadeOut("fast");
                                }

                                if (category_input_len < category_matched_len) {
                                    $("#category_id").val("");
                                    return false;
                                }

                            } else {
                                $("#category_id").val("");
                                return false;
                            }

                        }
                    } else {
                        $("#bdata_box").fadeOut("fast");
                    }
                });

                // click function
                $("#bdata_box option").click(function () {
                    var category_id = $(this).val();
                    var category_name = $(this).html();
                    $("#category").val(category_name);
                    $("#category_id").val(category_id);
                    $("#search_result").html('<a href="<?php echo url('/'); ?>/category_history_all/'+category_id+'">'+category_name+'</a>');
                    $("#bdata_box").fadeOut("fast");
                });
                
                


            });



            function toTitleCase(str)
            {
                return str.replace(/\w\S*/g, function (txt) {
                    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                });
            }
        </script>