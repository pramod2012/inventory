<script>
            var supplier_matched_result;
            var supplier_total_result;

            var supplier_matched_line;
            var supplier_matched_len;
            var supplier_input_len;


            $(document).ready(function () {

                $("#supplier").keyup(function () {
                    var supplier_name = $(this).val();
                    var cn = toTitleCase(supplier_name);
                    var cn_len = cn.length;

                    $("#bdata_box option").addClass("bdata_result");
                    $("#search_result").html("");

                    if (cn_len > 0) {

                        if (jQuery.trim(cn) != '') {

                            var supplier_matched_result = $("#bdata_box option:contains('" + cn + "')").removeClass("bdata_result");
                            $("#bdata_box").css("display", "inline-block");
                            $("#bdata_box").fadeIn("slow");
                            var supplier_total_result = supplier_matched_result.length;

                            if (supplier_total_result == 1) {
                                supplier_matched_line = supplier_matched_result.html();
                                supplier_matched_len = supplier_matched_line.length;
                                supplier_input_len = cn_len;
                                if (supplier_input_len === supplier_matched_len) {
                                    var supplier_id = supplier_matched_result.val();
                                    var supplier_name = supplier_matched_line;
                                    $("#supplier").val(supplier_name);
                                    $("#supplier_id").val(supplier_id);
                                    $("#search_result").html('<a href="<?php echo url('/'); ?>/supplier_history_all/'+supplier_id+'">'+supplier_name+'</a>');
                                    $("#bdata_box").fadeOut("fast");
                                }

                                if (supplier_input_len < supplier_matched_len) {
                                    $("#supplier_id").val("");
                                    return false;
                                }

                            } else {
                                $("#supplier_id").val("");
                                return false;
                            }

                        }
                    } else {
                        $("#bdata_box").fadeOut("fast");
                    }
                });

                // click function
                $("#bdata_box option").click(function () {
                    var supplier_id = $(this).val();
                    var supplier_name = $(this).html();
                    $("#supplier").val(supplier_name);
                    $("#supplier_id").val(supplier_id);
                    $("#search_result").html('<a href="<?php echo url('/'); ?>/supplier_history_all/'+supplier_id+'">'+supplier_name+'</a>');
                    $("#bdata_box").fadeOut("fast");
                });
                
                


            });



            function toTitleCase(str)
            {
                return str.replace(/\w\S*/g, function (txt) {
                    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                });
            }
        </script>