<script>
    $(document).ready(function(){
        
        //category function
        $("#category").focus(function(){
           $("#category_options").fadeIn("slow"); 
        }).blur(function(){
            $("#category_options").fadeOut("slow"); 
        });
        $("#category").keyup(function(){
            c_n = $(this).val();
            category_name = toTitleCase(c_n);
            category_name_length = category_name.length;
            
            $("#category_options option").addClass("show");
            
            if (category_name_length > 0) {
                if(jQuery.trim(category_name) != ''){
                    category_not_matched = $("#category_options option:not(:contains('" + category_name + "'))").removeClass("show").addClass("hide");
                    category_matched = $("#category_options option:contains('"+category_name+"')");
                    total_result = category_matched.length;
                    if(total_result == 1){
                        matched_line = category_matched.html();
                        category_id = category_matched.val();
                        matched_line_length = matched_line.length;
                        input_length = category_name_length;
                        if(matched_line_length == input_length){
                            $("#category").val(matched_line);
                            $("#category_id").val(category_id);
                            $("#category_options").fadeOut("fast");
                        } else {
                            $("#category_options").fadeIn("slow");
                        }
                    } 
                }
            }
        });
        //category option click function
        $("#category_options option").click(function(){
            category_name = $(this).html();
            category_id = $(this).val(); 
            $("#category").val(category_name);
            $("#category_id").val(category_id);
            $("#category_options").fadeOut("fast");
        });
        
        
        //brand function 
        //brand function
        $("#brand").focus(function(){
           $("#brand_options").fadeIn("slow"); 
        }).blur(function(){
            $("#brand_options").fadeOut("slow"); 
        });
        $("#brand").keyup(function(){
            c_n = $(this).val();
            brand_name = toTitleCase(c_n);
            brand_name_length = brand_name.length;
            
            $("#brand_options option").addClass("show");
            
            if (brand_name_length > 0) {
                if(jQuery.trim(brand_name) != ''){
                    brand_not_matched = $("#brand_options option:not(:contains('" + brand_name + "'))").removeClass("show").addClass("hide");
                    brand_matched = $("#brand_options option:contains('"+brand_name+"')");
                    total_result = brand_matched.length;
                    if(total_result == 1){
                        matched_line = brand_matched.html();
                        brand_id = brand_matched.val();
                        matched_line_length = matched_line.length;
                        input_length = brand_name_length;
                        if(matched_line_length == input_length){
                            $("#brand").val(matched_line);
                            $("#brand_id").val(brand_id);
                            $("#brand_options").fadeOut("fast");
                        } else {
                            $("#brand_options").fadeIn("slow");
                        }
                    } 
                }
            }
        });
        //brand option click function
        $("#brand_options option").click(function(){
            brand_name = $(this).html();
            brand_id = $(this).val(); 
            $("#brand").val(brand_name);
            $("#brand_id").val(brand_id);
            $("#brand_options").fadeOut("fast");
        });
        
        //product function
        $("#product").focus(function(){
           $("#product_options").fadeIn("slow"); 
        }).blur(function(){
            $("#product_options").fadeOut("slow"); 
        });
        $("#product").keyup(function(){
            c_n = $(this).val();
            product_name = toTitleCase(c_n);
            product_name_length = product_name.length;
            
            $("#product_options option").addClass("show");
            
            if (product_name_length > 0) {
                if(jQuery.trim(product_name) != ''){
                    product_not_matched = $("#product_options option:not(:contains('" + product_name + "'))").removeClass("show").addClass("hide");
                    product_matched = $("#product_options option:contains('"+product_name+"')");
                    total_result = product_matched.length;
                    if(total_result == 1){
                        matched_line = product_matched.html();
                        product_id = product_matched.val();
                        matched_line_length = matched_line.length;
                        input_length = product_name_length;
                        if(matched_line_length == input_length){
                            $("#product").val(matched_line);
                            $("#product_id").val(product_id);
                            $("#product_options").fadeOut("fast");
                        } else {
                            $("#product_options").fadeIn("slow");
                        }
                    } 
                }
            }
        });
        //product option click function
        $("#product_options option").click(function(){
            product_name = $(this).html();
            product_id = $(this).val(); 
            $("#product").val(product_name);
            $("#product_id").val(product_id);
            $("#product_options").fadeOut("fast");
        });
        
        //buyer function
        $("#buyer").focus(function(){
           $("#buyer_options").fadeIn("slow"); 
        }).blur(function(){
            $("#buyer_options").fadeOut("slow"); 
        });
        $("#buyer").keyup(function(){
            c_n = $(this).val();
            buyer_name = toTitleCase(c_n);
            buyer_name_length = buyer_name.length;
            
            $("#buyer_options option").addClass("show");
            
            if (buyer_name_length > 0) {
                if(jQuery.trim(buyer_name) != ''){
                    buyer_not_matched = $("#buyer_options option:not(:contains('" + buyer_name + "'))").removeClass("show").addClass("hide");
                    buyer_matched = $("#buyer_options option:contains('"+buyer_name+"')");
                    total_result = buyer_matched.length;
                    if(total_result == 1){
                        matched_line = buyer_matched.html();
                        buyer_id = buyer_matched.val();
                        matched_line_length = matched_line.length;
                        input_length = buyer_name_length;
                        if(matched_line_length == input_length){
                            $("#buyer").val(matched_line);
                            $("#buyer_id").val(buyer_id);
                            $("#buyer_options").fadeOut("fast");
                        } else {
                            $("#buyer_options").fadeIn("slow");
                        }
                    } 
                }
            }
        });
        //buyer option click function
        $("#buyer_options option").click(function(){
            buyer_name = $(this).html();
            buyer_id = $(this).val(); 
            $("#buyer").val(buyer_name);
            $("#buyer_id").val(buyer_id);
            $("#buyer_options").fadeOut("fast");
        });
        
        
        
        
        
//        Rate calculation
    $("#rate").keyup(function(){
        rate = $(this).val();
        quantity = $("#quantity").val();
        total = rate * quantity;
        $("#total").val(total);
        
    });
    
    $("#paid").keyup(function(){
        paid = $(this).val();
        total = $("#total").val();
        due = total - paid;
        $("#due").val(due);
    });
//      End rate calculation

    // Available product calculation
    
        
        
        
    });
    
    
    function toTitleCase(str)
    {
        return str.replace(/\w\S*/g, function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    }
</script>