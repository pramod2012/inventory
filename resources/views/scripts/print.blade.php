<script>
    function print_this(divName){
        var print_section = document.getElementById(divName).innerHTML;
        var page = document.body.innerHTML;
        var base_url = "<?php echo url('/'); ?>/";
        var css = document.getElementById("stylediv").innerHTML;
        document.body.innerHTML = css + print_section;
        window.print();
        document.body.innerHTML = page;
    }
</script>