<script>
            var buyer_matched_result;
            var buyer_total_result;

            var buyer_matched_line;
            var buyer_matched_len;
            var buyer_input_len;


            $(document).ready(function () {

                $("#buyer").keyup(function () {
                    var buyer_name = $(this).val();
                    var cn = toTitleCase(buyer_name);
                    var cn_len = cn.length;

                    $("#bdata_box option").addClass("bdata_result");
                    $("#search_result").html("");

                    if (cn_len > 0) {

                        if (jQuery.trim(cn) != '') {

                            var buyer_matched_result = $("#bdata_box option:contains('" + cn + "')").removeClass("bdata_result");
                            $("#bdata_box").css("display", "inline-block");
                            $("#bdata_box").fadeIn("slow");
                            var buyer_total_result = buyer_matched_result.length;

                            if (buyer_total_result == 1) {
                                buyer_matched_line = buyer_matched_result.html();
                                buyer_matched_len = buyer_matched_line.length;
                                buyer_input_len = cn_len;
                                if (buyer_input_len === buyer_matched_len) {
                                    var buyer_id = buyer_matched_result.val();
                                    var buyer_name = buyer_matched_line;
                                    $("#buyer").val(buyer_name);
                                    $("#buyer_id").val(buyer_id);
                                    $("#search_result").html('<a href="<?php echo url('/'); ?>/buyer_history_all/'+buyer_id+'">'+buyer_name+'</a>');
                                    $("#bdata_box").fadeOut("fast");
                                }

                                if (buyer_input_len < buyer_matched_len) {
                                    $("#buyer_id").val("");
                                    return false;
                                }

                            } else {
                                $("#buyer_id").val("");
                                return false;
                            }

                        }
                    } else {
                        $("#bdata_box").fadeOut("fast");
                    }
                });

                // click function
                $("#bdata_box option").click(function () {
                    var buyer_id = $(this).val();
                    var buyer_name = $(this).html();
                    $("#buyer").val(buyer_name);
                    $("#buyer_id").val(buyer_id);
                    $("#search_result").html('<a href="<?php echo url('/'); ?>/buyer_history_all/'+buyer_id+'">'+buyer_name+'</a>');
                    $("#bdata_box").fadeOut("fast");
                });
                
                


            });



            function toTitleCase(str)
            {
                return str.replace(/\w\S*/g, function (txt) {
                    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                });
            }
        </script>