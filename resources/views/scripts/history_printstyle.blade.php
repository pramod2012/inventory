<div id="stylediv">
    <style media="print">
        .stock_table{
    width: 96%;
    margin-left: auto;
    margin-right: auto;
    margin-top: 25px;
    margin-bottom: 25px;
}
.stock_table h3.title_three{
    font-size: 18px;
    color: #005280;
    margin-bottom: 18px;
}
.stock_table div.date{
    text-align: center;
    margin-bottom: 20px;
    color: #254d65;
}
.stock_table div.date span{
    color: #4590ce;
    padding: 0px 8px;
    text-transform: uppercase;
}
.stock_table table{
    width: 100%;
}
.stock_table table thead tr{
    height: 42px;
}
.stock_table table tbody tr{
    height: 32px;
}
.stock_table table tr th, .stock_table table tr td{
    border: 1px solid #afafaf;
}
.stock_table table tr th{
    background: #254d65;
    color: #efefef;
    font-family: corbel;
    text-align: center;
}
.stock_table table tr td{
    background: #f3f3f3;
    padding: 2px;
    font-family: Tahoma;
    text-align: center;
    font-size: 12px;
}
.stock_table table tr:last-child{
    font-weight: bold;
}
.stock_table table tr:not(:last-child) td:nth-child(7), .stock_table table tr:not(:last-child) td:nth-child(8), .stock_table table tr:not(:last-child) td:nth-child(9){
    text-align: right;
    padding-right: 6px;
}
.stock_table table tr:last-child td:nth-child(2), .stock_table table tr:last-child td:nth-child(3), .stock_table table tr:last-child td:nth-child(4){
    text-align: right;
    padding-right: 6px;
}
.stock_table table tr:last-child td:nth-child(1){
    text-align: right;
    padding-right: 16px;
}
.stock_table table tr td:last-child, .stock_table table tr th:last-child{
    display: none;
}
.stock_table table tr td img{
    width: 60px;
    border: 1px solid gray;
    vertical-align: middle;
    display: block;
    margin-left: auto;
    margin-right: auto;
    border-radius: 3px;
    background: white;
    padding: 3px;
}
.stock_table table tr td a#edit {
    padding: 3px 8px;
    background: #199680;
    color: white;
    border: 1px solid #0a6958;
    border-radius: 3px;
    font-size: 12px;
}
.stock_table table tr td a#delete {
    padding: 3px 8px;
    background: #ce5345;
    color: white;
    border: 1px solid #802d24;
    border-radius: 3px;
    font-size: 12px;
}
.signature{
    display: block;
        margin-top: 54px;
    }
    .signature div.party_one{
        text-decoration: overline;
        float: left;
    }
    .signature div.party_two{
        text-decoration: overline;
        float: right;
    }
    </style>
</div>