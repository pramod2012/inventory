<script>
            var brand_matched_result;
            var brand_total_result;

            var brand_matched_line;
            var brand_matched_len;
            var brand_input_len;


            $(document).ready(function () {

                $("#brand").keyup(function () {
                    var brand_name = $(this).val();
                    var cn = toTitleCase(brand_name);
                    var cn_len = cn.length;

                    $("#bdata_box option").addClass("bdata_result");
                    $("#search_result").html("");

                    if (cn_len > 0) {

                        if (jQuery.trim(cn) != '') {

                            var brand_matched_result = $("#bdata_box option:contains('" + cn + "')").removeClass("bdata_result");
                            $("#bdata_box").css("display", "inline-block");
                            $("#bdata_box").fadeIn("slow");
                            var brand_total_result = brand_matched_result.length;

                            if (brand_total_result == 1) {
                                brand_matched_line = brand_matched_result.html();
                                brand_matched_len = brand_matched_line.length;
                                brand_input_len = cn_len;
                                if (brand_input_len === brand_matched_len) {
                                    var brand_id = brand_matched_result.val();
                                    var brand_name = brand_matched_line;
                                    $("#brand").val(brand_name);
                                    $("#brand_id").val(brand_id);
                                    $("#search_result").html('<a href="<?php echo url('/'); ?>/brand_history_all/'+brand_id+'">'+brand_name+'</a>');
                                    $("#bdata_box").fadeOut("fast");
                                }

                                if (brand_input_len < brand_matched_len) {
                                    $("#brand_id").val("");
                                    return false;
                                }

                            } else {
                                $("#brand_id").val("");
                                return false;
                            }

                        }
                    } else {
                        $("#bdata_box").fadeOut("fast");
                    }
                });

                // click function
                $("#bdata_box option").click(function () {
                    var brand_id = $(this).val();
                    var brand_name = $(this).html();
                    $("#brand").val(brand_name);
                    $("#brand_id").val(brand_id);
                    $("#search_result").html('<a href="<?php echo url('/'); ?>/brand_history_all/'+brand_id+'">'+brand_name+'</a>');
                    $("#bdata_box").fadeOut("fast");
                });
                
                


            });



            function toTitleCase(str)
            {
                return str.replace(/\w\S*/g, function (txt) {
                    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                });
            }
        </script>