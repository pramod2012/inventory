<div id="stylediv">
    <style media="print">
    #stock_report, #sell_report{
    width: 80%;
    margin-left: auto;
    margin-right: auto;
    background: white;
    border-radius: 2px;
    margin-top: 25px;
    margin-bottom: 25px;
    padding: 32px;
}
#stock_report h3.title_three, #sell_report h3.title_three{
    
}
#stock_report h3.title_three span, #sell_report h3.title_three span{
    color: #2f7979;
}
#stock_report h3.supplier, #sell_report h3.buyer{
    text-align: left;
    margin-top: 25px;
    margin-bottom: 10px;
    font-size: 16px;
}
#stock_report .date, #sell_report .date{
        text-align: center;
}
#stock_report table, #sell_report table{
    min-width: 300px;
}
#stock_report table tr, #sell_report table tr{
    
}
#stock_report table tr:nth-child(even), #sell_report table tr:nth-child(even){
    background: #fcfcff;
}
#stock_report table tr:nth-child(odd), #sell_report table tr:nth-child(odd){
    background: #f7fdf7;
}
#stock_report table tr th, #stock_report table tr td, 
#sell_report table tr th, #sell_report table tr td{
    border: 1px solid #e0e0e0;
    padding: 6px 11px;
}
#stock_report table tr th, #sell_report table tr th{
    font-weight: normal;
    color: #3a3394;
}
#stock_report table tr td, #sell_report table tr td{
    text-align: right;
}
.signature{
    display: block;
        margin-top: 54px;
    }
    .signature div.party_one{
        text-decoration: overline;
        float: left;
    }
    .signature div.party_two{
        text-decoration: overline;
        float: right;
    }
</style>
</div>