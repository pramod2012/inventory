<script>
            var product_matched_result;
            var product_total_result;

            var product_matched_line;
            var product_matched_len;
            var product_input_len;


            $(document).ready(function () {

                $("#product").keyup(function () {
                    var product_name = $(this).val();
                    var cn = toTitleCase(product_name);
                    var cn_len = cn.length;

                    $("#bdata_box option").addClass("bdata_result");
                    $("#search_result").html("");

                    if (cn_len > 0) {

                        if (jQuery.trim(cn) != '') {

                            var product_matched_result = $("#bdata_box option:contains('" + cn + "')").removeClass("bdata_result");
                            $("#bdata_box").css("display", "inline-block");
                            $("#bdata_box").fadeIn("slow");
                            var product_total_result = product_matched_result.length;

                            if (product_total_result == 1) {
                                product_matched_line = product_matched_result.html();
                                product_matched_len = product_matched_line.length;
                                product_input_len = cn_len;
                                if (product_input_len === product_matched_len) {
                                    var product_id = product_matched_result.val();
                                    var product_name = product_matched_line;
                                    $("#product").val(product_name);
                                    $("#product_id").val(product_id);
                                    $("#search_result").html('<a href="<?php echo url('/'); ?>/product_history_all/'+product_id+'">'+product_name+'</a>');
                                    $("#bdata_box").fadeOut("fast");
                                }

                                if (product_input_len < product_matched_len) {
                                    $("#product_id").val("");
                                    return false;
                                }

                            } else {
                                $("#product_id").val("");
                                return false;
                            }

                        }
                    } else {
                        $("#bdata_box").fadeOut("fast");
                    }
                });

                // click function
                $("#bdata_box option").click(function () {
                    var product_id = $(this).val();
                    var product_name = $(this).html();
                    $("#product").val(product_name);
                    $("#product_id").val(product_id);
                    $("#search_result").html('<a href="<?php echo url('/'); ?>/product_history_all/'+product_id+'">'+product_name+'</a>');
                    $("#bdata_box").fadeOut("fast");
                });
                
                


            });



            function toTitleCase(str)
            {
                return str.replace(/\w\S*/g, function (txt) {
                    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                });
            }
        </script>