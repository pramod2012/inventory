@extends('home')

@section('maincontent')
<div id="home_content">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-1">
            <div class="home_box">
                <a href="<?php echo url('/'); ?>/stock">Stock</a>
            </div>
            <div class="home_box">
                <a href="<?php echo url('/'); ?>/sell">Sell</a>
            </div>
            <div class="home_box">
                <a href="<?php echo url('/'); ?>/available_stock">Available Stock</a>
            </div>
            <div class="home_box">
                <a href="<?php echo url('/'); ?>/stock_history">Stock HIstory</a>
            </div>
            <div class="home_box">
                <a href="<?php echo url('/'); ?>/sell_history">Sell HIstory</a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-1 col-lg-4 col-lg-offset-1">
            <script>
                var category_matched_result;
                var category_total_result;

                var category_matched_line;
                var category_matched_len;
                var category_input_len;

                //brand part
                var brand_matched_result;
                var brand_total_result;

                var brand_matched_line;
                var brand_matched_len;
                var brand_input_len;

                //product part
                var product_matched_result;
                var product_total_result;

                var product_matched_line;
                var product_matched_len;
                var product_input_len;

                $(document).ready(function () {

                    $("#category").keyup(function () {
                        var category_name = $(this).val();
                        var cn = toTitleCase(category_name);
                        var cn_len = cn.length;

                        $("#cdata_box option").addClass("cdata_result");

                        if (cn_len > 0) {

                            if (jQuery.trim(cn) != '') {

                                var category_matched_result = $("#cdata_box option:contains('" + cn + "')").removeClass("cdata_result");
                                $("#cdata_box").css("display", "inline-block");
                                $("#cdata_box").fadeIn("slow");
                                var category_total_result = category_matched_result.length;

                                if (category_total_result == 1) {
                                    category_matched_line = category_matched_result.html();
                                    category_matched_len = category_matched_line.length;
                                    category_input_len = cn_len;
                                    if (category_input_len === category_matched_len) {
                                        var category_id = category_matched_result.val();
                                        var category_name = category_matched_line;
                                        $("#category").val(category_name);
                                        $("#category_id").val(category_id);
                                        $("#cdata_box").fadeOut("fast");
                                    }

                                    if (category_input_len < category_matched_len) {
                                        $("#category_id").val("");
                                        return false;
                                    }

                                } else {
                                    $("#category_id").val("");
                                    return false;
                                }

                            }
                        } else {
                            $("#cdata_box").fadeOut("fast");
                        }
                    });

                    // click function
                    $("#cdata_box option").click(function () {
                        var category_id = $(this).val();
                        var category_name = $(this).html();
                        $("#category").val(category_name);
                        $("#category_id").val(category_id);
                        $("#cdata_box").fadeOut("fast");
                    });

                    //Brand function
                    $("#brand").keyup(function () {
                        var brand_name = $(this).val();
                        var cn = toTitleCase(brand_name);
                        var cn_len = cn.length;

                        $("#bdata_box option").addClass("bdata_result");

                        if (cn_len > 0) {

                            if (jQuery.trim(cn) != '') {

                                var brand_matched_result = $("#bdata_box option:contains('" + cn + "')").removeClass("bdata_result");
                                $("#bdata_box").css("display", "inline-block");
                                $("#bdata_box").fadeIn("slow");
                                var brand_total_result = brand_matched_result.length;

                                if (brand_total_result == 1) {
                                    brand_matched_line = brand_matched_result.html();
                                    brand_matched_len = brand_matched_line.length;
                                    brand_input_len = cn_len;
                                    if (brand_input_len === brand_matched_len) {
                                        var brand_id = brand_matched_result.val();
                                        var brand_name = brand_matched_line;
                                        $("#brand").val(brand_name);
                                        $("#brand_id").val(brand_id);
                                        $("#bdata_box").fadeOut("fast");
                                    }

                                    if (brand_input_len < brand_matched_len) {
                                        $("#brand_id").val("");
                                        return false;
                                    }

                                } else {
                                    $("#brand_id").val("");
                                    return false;
                                }

                            }
                        } else {
                            $("#bdata_box").fadeOut("fast");
                        }
                    });

                    // click function
                    $("#bdata_box option").click(function () {
                        var brand_id = $(this).val();
                        var brand_name = $(this).html();
                        $("#brand").val(brand_name);
                        $("#brand_id").val(brand_id);
                        $("#bdata_box").fadeOut("fast");
                    });

                    //product function

                    $("#product").keyup(function () {
                        var product_name = $(this).val();
                        var cn = toTitleCase(product_name);
                        var cn_len = cn.length;

                        $("#pdata_box option").addClass("pdata_result");

                        if (cn_len > 0) {

                            if (jQuery.trim(cn) != '') {

                                var product_matched_result = $("#pdata_box option:contains('" + cn + "')").removeClass("pdata_result");
                                $("#pdata_box").css("display", "inline-block");
                                $("#pdata_box").fadeIn("slow");
                                var product_total_result = product_matched_result.length;

                                if (product_total_result == 1) {
                                    product_matched_line = product_matched_result.html();
                                    product_matched_len = product_matched_line.length;
                                    product_input_len = cn_len;
                                    if (product_input_len === product_matched_len) {
                                        var product_id = product_matched_result.val();
                                        var product_name = product_matched_line;
                                        $("#product").val(product_name);
                                        $("#product_id").val(product_id);
                                        $("#pdata_box").fadeOut("fast");
                                    }

                                    if (product_input_len < product_matched_len) {
                                        $("#product_id").val("");
                                        return false;
                                    }

                                } else {
                                    $("#product_id").val("");
                                    return false;
                                }

                            }
                        } else {
                            $("#pdata_box").fadeOut("fast");
                        }
                    });

                    // click function
                    $("#pdata_box option").click(function () {
                        var product_id = $(this).val();
                        var product_name = $(this).html();
                        $("#product").val(product_name);
                        $("#product_id").val(product_id);
                        $("#pdata_box").fadeOut("fast");
                    });


                });



                function toTitleCase(str)
                {
                    return str.replace(/\w\S*/g, function (txt) {
                        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                    });
                }
            </script>
            <div class="search_form">
                <h4 class="title_four">Search Product in Stock</h4>
                <form action="<?php echo url('/'); ?>/search" method="post">
                    <label for="category">Category</label>
                    <input type="text" id="category" placeholder="Category" autocomplete="off">
                    <input type="hidden" name="category_id" value="" id="category_id"/>
                    <div id="cdata_box">

                        <?php foreach ($categories as $c) { ?>
                            <option value="<?php echo $c->category_id; ?>"><?php echo $c->category_name; ?></option>
                        <?php } ?>

                    </div>
                    <label>Brand</label>
                    <input type="text" placeholder="Brand Name" id="brand" autocomplete="off">
                    <input type="hidden" name="brand_id" value="" id="brand_id" >
                    <div id="bdata_box">
                        <?php foreach ($brands as $b) { ?>
                            <option value="<?php echo $b->brand_id; ?>"><?php echo $b->brand_name; ?></option>
                        <?php } ?>
                    </div>

                    <label>Product</label>
                    <input type="text" id="product" placeholder="Product Name"  autocomplete="off">
                    <input type="hidden" name="product_id" value="" id="product_id" >
                    <div id="pdata_box">
                        <?php foreach ($products as $p) { ?>
                            <option value="<?php echo $p->product_id; ?>"><?php echo $p->product_name; ?></option>
                        <?php } ?>
                    </div>
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
                    <input type="submit" value="Search">

                </form>
            </div>
            <?php 
            $total_stock_due = DB::table('tbl_stock')->where('stock_due_amount', '!=', '0')->sum('stock_due_amount');
            $total_pay_due = DB::table('tbl_stock')->where('pay_due', '!=', '0')->sum('pay_due');
            $total_due = $total_stock_due - $total_pay_due;
            
            $total_sell_due = DB::table('tbl_sell')->where('sell_due_price', '!=', '0')->sum('sell_due_price');
            $total_receive_due = DB::table('tbl_sell')->where('receive_due', '!=', '0')->sum('receive_due');
            $total_receivable = $total_sell_due - $total_receive_due;
            
            $y = date('Y');
            $m = date('m');
            $total_stock_due_this_month = DB::table('tbl_stock')
                    ->where('stock_due_amount', '!=', '0')
                    ->whereYear('stock_date', $y)
                    ->whereMonth('stock_date', $m)
                    ->sum('stock_due_amount');
            
            $total_pay_due_this_month = DB::table('tbl_stock')
                    ->where('pay_due', '!=', '0')
                    ->whereYear('stock_date', $y)
                    ->whereMonth('stock_date', $m)
                    ->sum('pay_due');
            $total_payable_this_month = $total_stock_due_this_month - $total_pay_due_this_month;
            
            $total_sell_due_this_month = DB::table('tbl_sell')
                    ->where('sell_due_price', '!=', '0')
                    ->whereYear('sell_date', $y)
                    ->whereMonth('sell_date', $m)
                    ->sum('sell_due_price');
            $total_receive_due_this_month = DB::table('tbl_sell')
                    ->where('receive_due', '!=', '0')
                    ->whereYear('sell_date', $y)
                    ->whereMonth('sell_date', $m)
                    ->sum('receive_due');
            $total_receivable_this_month = $total_sell_due_this_month - $total_receive_due_this_month;
            ?>
            <div class="stat_box">
                <h4 class="title_four">Statistics</h4>
                <ul>
                    <li>Total Payable <span><?php echo $total_due; ?></span></li>
                    <li>Total Receivable <span><?php echo $total_receivable; ?></span></li>
                    <li>This Month Payable <span><?php echo $total_payable_this_month; ?></span></li>
                    <li>This month Receivable <span><?php echo $total_receivable_this_month; ?></span></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection