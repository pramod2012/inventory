<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Admin Login - Laravel Accounting App</title>

    <meta name="author" content="Mohammad Kawser">
    <!-- Bootstrap -->
    <link href="<?php echo url('/'); ?>/asset/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo url('/'); ?>/asset/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo url('/'); ?>/asset/css/login_1.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

    <div id="login_box">
        <h1>Admin Login</h1>
        <?php 
            if(Session::has('message')){
                echo Session::get('message');
            }
        ?>
        <form action="<?php echo url('/'); ?>/login" method="post">
            <label for="email">Email</label>
            <input type="email" name="email" id="email">
            <label for="password">Password</label>
            <input type="password" name="password" id="password">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
            <input type="submit" value="Login">
        </form>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo url('/'); ?>/asset/js/bootstrap.min.js"></script>
</body>
</html>