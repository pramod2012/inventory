<style>
    .stock_table table tr:last-child {
        font-weight: normal;
    }
    .stock_table table tr:last-child td:nth-child(2),
    .stock_table table tr:not(:last-child) td:nth-child(9),
    .stock_table table tr:not(:last-child) td:nth-child(8),
    .stock_table table tr:not(:last-child) td:nth-child(7),
    .stock_table table tr:last-child td:nth-child(1), .stock_table table tr:last-child td:nth-child(3), .stock_table table tr:last-child td:nth-child(4) {
        text-align: center;
        padding-right: 0px;
    }
</style>