@extends('home')

@section('maincontent')

<div class="simple_form">
    <?php if($count < 1) { ?>
    <form action="<?php echo url('/'); ?>/logo" method="post" enctype="multipart/form-data">
        <label>Change The logo</label>
        <input type="file" name="logo" placeholder="App logo" >
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
        <input type="submit" value="Save" >
    </form>
    <?php } else if ($count == 1) { ?>
    <form action="<?php echo url('/'); ?>/logo" method="post" enctype="multipart/form-data">
        <label>Update The logo</label>
        <input type="file" name="logo" value="<?php echo $current_logo->logo; ?>" >
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
        <input type="hidden" name="_method" value="PUT" >
        <input type="hidden" name="logo_id" value="<?php echo $current_logo->logo_id; ?>" >
        <input type="submit" value="Update" >
    </form>
    <label>Current logo</label>
    <img style="height: 100px; width: 100px" src="<?php echo url('/'); ?>/<?php echo $current_logo->logo; ?>" >
    <?php } ?>
</div>

@endsection