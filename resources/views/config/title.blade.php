@extends('home')

@section('maincontent')

<div class="simple_form">
    <?php if($count < 1) { ?>
    <form action="<?php echo url('/'); ?>/title" method="post">
        <label>Change The Title</label>
        <input type="text" name="title" placeholder="App Title" >
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
        <input type="submit" value="Save" >
    </form>
    <?php } else if ($count == 1) { ?>
    <form action="<?php echo url('/'); ?>/title" method="post">
        <label>Update The Title</label>
        <input type="text" name="title" value="<?php echo $current_title->title; ?>" >
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
        <input type="hidden" name="_method" value="PUT" >
        <input type="hidden" name="title_id" value="<?php echo $current_title->title_id; ?>" >
        <input type="submit" value="Update" >
    </form>
    <?php } ?>
</div>

@endsection