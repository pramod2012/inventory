@extends('home')

@section('maincontent')
<h2 class="title_two">Edit Product</h2>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div class="simple_form">
            <?php 
            if(Session::has('message')){
                echo Session::get('message');
            }
            ?>
            <form action="<?php echo url('/'); ?>/update_product" method="post">
                <label for="pn">Product Name</label>
                <input type="text" name="product_name" id="pn" value="<?php echo $product->product_name; ?>">
                <input type="hidden" name="product_id" value="<?php echo $product->product_id; ?>" >
                <label for="bd">Product Description</label>
                <textarea name="product_description"><?php echo $product->product_description; ?></textarea>
                <input type="hidden" name="_method" value="PUT" >
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
                <input type="submit" value="Update">
            </form>
            
            <br>
            <br>
            
            <?php 
            $product_image = $product->product_image;
            if(strlen($product_image) > 3) {
            ?>
            <form action="<?php echo url('/'); ?>/update_product_image" method="post" enctype="multipart/form-data">
                <img style="width: 100px; height: 100px; border:1px solid black; border-radius: 2px;" src="<?php echo url('/'); ?>/<?php echo $product->product_image; ?>" >
                <input type="hidden" name="product_id" value="<?php echo $product->product_id; ?>" >
                <label for="pn">Change Product Image</label>
                <input type="file" name="product_image" id="pn">
                <input type="hidden" name="_method" value="PUT" >
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
                <input type="submit" value="Update">
            </form>
            <?php } ?>
        </div>
    </div>
    
</div>


@endsection