@extends('home')

@section('maincontent')
@include('scripts.product_search')
<h2 class="title_two"><?php echo $title; ?></h2>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div class="simple_form">
            <?php 
            if(Session::has('message')){
                echo Session::get('message');
            }
            ?>
            <form action="<?php echo url('/'); ?>/add_product" method="post" enctype="multipart/form-data">
                <label for="pn">Product Name</label>
                <input type="text" name="product_name" id="pn" placeholder="Product Name">
                <label for="pn">Product Image</label>
                <input type="file" name="product_image" id="pn" placeholder="Product Image">
                <label for="bd">Product Description</label>
                <textarea name="product_description" placeholder="Product Description"></textarea>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
                <input type="submit" value="Add">
            </form>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div class="search_form">
            <h4 class="title_four">Search Product</h4>
            <form action="<?php echo url('/'); ?>/product_history" method="post">
                <label for="product">Product</label>
                <input type="text" id="product" placeholder="Product Name" autocomplete="off">
                <span id="search_result">
                    
                </span>
                <input type="hidden" name="product_id" value="" id="product_id"/>
                <div id="bdata_box">

                    <?php foreach ($all_products as $p) { ?>
                        <option value="<?php echo $p->product_id; ?>"><?php echo $p->product_name; ?></option>
                    <?php } ?>

                </div>
                
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
<!--                <input type="submit" value="Search" >-->
            </form>
        </div>
        <div class="basic_table">
            <table>
                <tr>
                    <th>Product Image</th>
                    <th>Product Name</th>
                    <th>Action</th>
                </tr>
                <?php foreach($products as $p) { ?>
                <tr>
                    <td class="product_image">
                        <?php 
                        $product_image = $p->product_image;
                        if(strlen($product_image) > 3) { ?>
                        <img src="<?php echo url('/'); ?>/<?php echo $product_image; ?>" >
                        <?php } ?>
                    </td>
                    <td>
                        <?php echo $p->product_name; ?>
                        (<?php 
                        $available = DB::table('tbl_stock')
                                ->where('product_id', $p->product_id)
                                ->sum('quantity_remains');
                        echo $available;
                        ?>)
                    </td>
                    <td>
                        <a id="edit" href="<?php echo url('/'); ?>/edit_product/<?php echo $p->product_id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <a id="delete" href="<?php  echo url('/'); ?>/delete_product/<?php echo $p->product_id; ?>" onclick="Confirm.check('Confirm !', 'Are you sure to delete this?'); return false;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        <!-- <?php  echo url('/'); ?>/delete_product/<?php echo $p->product_id; ?> -->
                    </td>
                </tr>
                <?php } ?>
            </table>
            <div class="page_links">
                <?php echo $products->links(); ?>
            </div>
        </div>
    </div>
</div>





@endsection