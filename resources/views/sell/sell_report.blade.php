@extends('home')

@section('maincontent')
@include('scripts.printstyle')
<?php

$sell_date = $sell->sell_date;
$buyer_id = $sell->buyer_id;
$product_id = $sell->product_id;
$product_quantity = $sell->sell_quantity;
$product_rate = $sell->sell_rate;
$price_total = $sell->sell_total_price;
$received = $sell->sell_received_price;
$due = $sell->sell_due_price;

$buyer = DB::table('tbl_buyer')
        ->where('buyer_id', $buyer_id)
        ->first();
$buyer_name = $buyer->buyer_name;
$product = DB::table('tbl_product')
        ->where('product_id', $product_id)
        ->first();
$product_name = $product->product_name;

$product_image = $product->product_image;
$site_title = DB::table('tbl_title')
        ->orderBy('title_id', 'desc')
        ->first();
$t = $site_title->title;
?>
@include('scripts.print')
<div id="report">
<div id="sell_report">
    <h3 class="title_three"><?php echo $t; ?></h3>
    <div class="date">1 january 2017</div>

    <h3 class="title_three buyer"><span>Buyer: </span><?php echo $buyer_name; ?></h3>
    <table>
        <tr>
            <th>Product Name</th><td><?php echo $product_name; ?></td>
        </tr>
        <tr>
            <th>Product Quantity</th><td><?php echo $product_quantity; ?></td>
        </tr>
        <tr>
            <th>Product Rate</th><td><?php echo $product_rate; ?></td>
        </tr>
        <tr>
            <th>Total Price</th><td><?php echo $price_total; ?></td>
        </tr>
        <tr>
            <th>Paid</th><td><?php echo $received; ?></td>
        </tr>
        <tr>
            <th>Due</th><td><?php echo $price_total - $received; ?></td>
        </tr>
    </table>
    <div class="signature">
        <div class="party_one">Company Sign</div>
        <div class="party_two">Buyer Sign</div>
        <span style="clear: both;"></span>
    </div>
<!--    <div class="footer_link">
        <a href="<?php echo url('/'); ?>">Home</a>
        <a href="<?php echo url(''); ?>/sell">Buyer</a>
    </div>-->
</div>
</div>
    <div id="print_button">
        <a href="#" onclick="return print_this('report');">Print</a>
    </div>

@endsection