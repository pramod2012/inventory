@extends('home')

@section('maincontent')
@include('scripts.printstyle')
<?php

$sell_date = $sell->sell_date;
$buyer_id = $sell->buyer_id;

$due_total = $sell->due_total;
$receive_due = $sell->receive_due;

$buyer = DB::table('tbl_buyer')
        ->where('buyer_id', $buyer_id)
        ->first();
$buyer_name = $buyer->buyer_name;

$site_title = DB::table('tbl_title')
        ->orderBy('title_id', 'desc')
        ->first();
$t = $site_title->title;
?>
@include('scripts.print')
<div id="report">
<div id="sell_report">
    <h3 class="title_three"><?php echo $t; ?></h3>
    <div class="date"><?php echo $sell_date; ?></div>

    <h3 class="title_three buyer"><span>Buyer: </span><?php echo $buyer_name; ?></h3>
    <table>
        <tr>
            <th>Total Due</th><td><?php echo $due_total; ?></td>
        </tr>
        <tr>
            <th>Due Received</th><td><?php echo $receive_due; ?></td>
        </tr>
        <tr>
            <th>Receivable</th><td><?php echo $due_total - $receive_due ?></td>
        </tr>
    </table>
    
    <div class="signature">
        <div class="party_one">Company Sign</div>
        <div class="party_two">Buyer Sign</div>
        <span style="clear: both;"></span>
    </div>
<!--    <div class="footer_link">
        <a href="<?php echo url('/'); ?>">Home</a>
        <a href="<?php echo url(''); ?>/sell">Buyer</a>
    </div>-->
</div>
</div>
    <div id="print_button">
        <a href="#" onclick="return print_this('report');">Print</a>
    </div>

@endsection