@extends('home')

@section('maincontent')
@include('scripts.sell_script')

<h2 class="title_two"><?php echo $title; ?></h2>
<div class="row">

    <div class="basic_form">
        <?php 
        if(Session::has('message')){
            echo Session::get('message');
        }
        ?>
        <form action="<?php echo url('/'); ?>/sell" method="post">
            <div id="inventory_head">
<!--                <input id="p" type="radio" name="inventory_type" value="p">
                <label for="p">Purchase</label>

                <input id="s" type="radio" name="inventory_type" value="s">
                <label for="s">Sell</label>-->


                
                <div id="part">
                    <label>Select Category</label>
                    <input type="text" id="category" placeholder="Category Name" autocomplete="off">
                    <input type="hidden" name="category_id" id="category_id" >
                    <div id="category_options">
                        <option value="0">--Select Category--</option>
                        <?php foreach($categories as $c) { ?>
                        <option value="<?php echo $c->category_id; ?>"><?php echo $c->category_name; ?></option>
                        <?php } ?>
                    </div>
                </div>
                
                <div id="part">
                    <label>Select Brand</label>
                    <input type="text" id="brand" placeholder="Brand Name" autocomplete="off">
                    <input type="hidden" name="brand_id" id="brand_id" >
                    <div id="brand_options">
                        <option value="0">--Select Brand--</option>
                        <?php foreach($brands as $b) { ?>
                        <option value="<?php echo $b->brand_id; ?>"><?php echo $b->brand_name; ?></option>
                        <?php } ?>
                    </div>
                </div>
                
                <div id="part">
                    <label>Select Product</label>
                    <input type="text" id="product" placeholder="Product Name" autocomplete="off">
                    <input type="hidden" name="product_id" id="product_id" >
                    <div id="product_options">
                        <option value="0">--Select Product--</option>
                        <?php foreach($products as $p) { ?>
                        <option value="<?php echo $p->product_id; ?>"><?php echo $p->product_name; ?></option>
                        <?php } ?>
                    </div>
                </div>

                <script>
                $(document).ready(function(){
                   $(document).on("click", "#check_products", function(e){
                       category_id = $("#category_id").val();
                       brand_id = $("#brand_id").val();
                       product_id = $("#product_id").val();
                       $.ajax({
                          type: "GET",
                          url: "<?php echo url('/'); ?>/check_stock/"+category_id+'/'+brand_id+'/'+product_id,
                          success:function(result){
                              $("#result").html("available " + result);
                          }
                          
                       });
                       return false;
                   });
                });
                </script>
                <div id="part" style="line-height: 72px;">
                    <button id="check_products">Check</button>
                </div>
                <div id="part" style="line-height: 72px;">
                    <div id="result"></div>
                </div>
                <div style="clear: both;"></div>
                <br>
                
                <div id="part2">
                    <label>Sell as</label>
                </div>
                <div id="part2">
                    <input id="p" type="radio" name="sell_type" value="f">
                    <label for="p">First in first out</label>
                </div>
                <div id="part2">
                    <input id="s" type="radio" name="sell_type" value="l">
                    <label for="s">Last in first out</label>
                </div>
                <br>
                
            </div>
            <!--  -->
<!--            <div class="form_section">
                <label>Product Name</label>
                <input type="text" name="product_name" placeholder="Product Name">
            </div>-->

              
            <div class="form_section">
                <label>Select Buyer</label>
                <input type="text" id="buyer" placeholder="Buyer Name" autocomplete="off">
                <input type="hidden" name="buyer_id" id="buyer_id" >
                <div id="buyer_options">
                    <option value="0">--Select Buyer--</option>
                    <?php foreach($buyers as $s) { ?>
                    <option value="<?php echo $s->buyer_id; ?>"><?php echo $s->buyer_name; ?></option>
                    <?php } ?>
                </div>
            </div>
            <div class="form_section">
                <label>Product Quantity</label>
                <input type="number" name="sell_quantity" id="quantity" placeholder="Product Quantity">
            </div>
            <div class="form_section">
                <label>Rate</label>
                <input type="number" name="sell_rate" id="rate" placeholder="Each Product Rate">
            </div>
            <div class="form_section">
                <label>Total</label>
                <input type="number" name="sell_total_price" id="total" placeholder="Total Price" disabled="disabled">
            </div>
            <div class="form_section">
                <label>Received</label>
                <input type="number" name="sell_received_price" id="paid" placeholder="Received">
            </div>
            <div class="form_section">
                <label>Due</label>
                <input type="number" name="sell_due_price" id="due" placeholder="Due" disabled="disabled">
            </div>
            <div class="form_section">
                <label>Date</label>
                <input type="date" name="sell_date" value="">
            </div>
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
            
            <div class="form_section">
                <input type="submit" value="Sell" >
            </div>
            
            <div style="clear: both;"></div>
        </form>
    </div>

</div>





@endsection
