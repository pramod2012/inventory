@extends('home')

@section('maincontent')
@include('style.stock_overwrite')
<div class="row">
    <h2 class="title_two"><?php echo $title; ?></h2>
    
    <div class="stock_table">
        <table>
            <thead>
                <tr>
                    <th>Date</th>
                    <th>buyer</th>
                    <th>Image</th>
                    <th>Brand</th>
                    <th>Name</th>
                    <th>Quantity</th>
                    <th>Rate</th>
                    <th>Total</th>
<!--                    <th>Receive</th>
                    <th>Due/Receivable</th>-->
                    <th>Action</th>
                </tr>
            </thead>
            
            <tbody>
                <?php foreach($sells as $sell) { ?>
                <tr>
                    <td><?php echo $sell->sell_date; ?></td>
                    <td><?php echo $sell->buyer_name; ?></td>
                    <td>
                        <?php 
                        $img = $sell->product_image;
                        if(strlen($img) > 3){ ?>
                            <img src="<?php echo url('/'); ?>/<?php echo $img; ?>" >
                        <?php } else {  ?>
                            Not Available
                        <?php } ?>
                    </td>
                    <td><?php echo $sell->brand_name; ?></td>
                    <td><?php echo $sell->product_name; ?></td>
                    <td><?php echo $sell->sell_quantity; ?></td>
                    <td><?php echo $sell->sell_rate; ?></td>
                    <td><?php echo $sell->sell_total_price; ?></td>
<!--                    <td><?php echo $sell->sell_received_price; ?></td>
                    <td><?php echo $sell->sell_due_price; ?></td>-->
                    <td>
                        <a id="edit" href="<?php echo url('/'); ?>/"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <a id="delete" href="<?php  echo url('/'); ?>/" onclick="Confirm.check('Confirm !', 'Are you sure to delete this?'); return false;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <div class="page_links">
            <?php echo $sells->links(); ?>
        </div>
        
    </div>
</div>

@endsection

