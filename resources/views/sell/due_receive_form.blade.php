@extends('home')

@section('maincontent')

<h2 class="title_two"><?php echo $title; ?> from: <?php echo $buyer; ?></h2>
<div class="row">

    <div class="basic_form">
        <?php 
        if(Session::has('message')){
            echo Session::get('message');
        }
        ?>
        <script>
        $(document).ready(function(){
            $("#receive_due").keyup(function(){
               var paid = $(this).val();
               var total = "<?php echo $due_total; ?>";
               var remain = total - paid;
               $("#pay_status").fadeIn("fast");
               $("#due_remain").html(remain);
            });
        });
        </script>
        <form action="<?php echo url('/'); ?>/save_due" method="post">
            <h5>Total Due: <?php echo $due_total; ?></h5>
            <div class="form_section">
                <label for="receive_due">Due Received</label>
                <input type="number" name="receive_due" id="receive_due" placeholder="Due Received">
            </div>
            <div id="pay_status">Due will remain: <span id="due_remain"></span></div>
            <div class="form_section">
                <label>Date</label>
                <input type="date" name="sell_date" value="">
            </div>
            <input type="hidden" name="due_total" value="<?php echo $due_total; ?>" >
            <input type="hidden" name="buyer_id" value="<?php echo $buyer_id; ?>" >
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
            
            <div class="form_section">
                <input type="submit" value="Save" >
            </div>
            
            <div style="clear: both;"></div>
        </form>
    </div>

</div>







@endsection
