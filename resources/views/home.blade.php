<?php 
    $site_title = DB::table('tbl_title')
            ->orderBy('title_id', 'desc')
            ->first();
    $logo = DB::table('tbl_logo')
            ->orderBy('logo_id', 'desc')
            ->first();
    $t = $site_title->title;
    $l = $logo->logo;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>
        <?php 
        if(isset($title) && !empty($title)){
            echo $title;
        } else {
            echo "Laravel Inventory App";
        }
        ?>
    </title>
    <link rel="shortcut icon" href="<?php echo url('/'); ?>/asset/images/icon.png" >
    <meta name="author" content="Mohammad Kawser">
    <!-- Bootstrap -->
    <link href="<?php echo url('/'); ?>/asset/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo url('/'); ?>/asset/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo url('/'); ?>/asset/css/style_1.css">
    <link rel="stylesheet" type="text/css" href="<?php echo url('/'); ?>/asset/css/responsive.css">
    <link rel="stylesheet" type="text/css" href="<?php echo url('/'); ?>/asset/css/menu.css">
    <link rel="stylesheet" type="text/css" href="<?php echo url('/'); ?>/asset/css/custom_confirm.css">
    <link href="https://fonts.googleapis.com/css?family=Indie+Flower|Dosis|Sansita|Supermercado+One" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    
    
    
</head>
<body>
    @include('includes.confirm')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
            <header>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="header_left">
                            <img src="<?php echo url('/'); ?>/<?php echo $l; ?>" alt="logo" >
                            <h1 class="header_title">
                                <a href="#"><?php echo $t; ?></a>
                            </h1>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="header_right">
                            <?php if(Session::has('admin_login')) { ?>
                            <a href="<?php echo url('/'); ?>/add_staff">Staff</a>
                            <a href="<?php echo url('/'); ?>/config">Configuration</a>
                            <?php } ?>
                            <a href="<?php echo url('/'); ?>/settings">Settings</a>
                            <a href="<?php echo url('/'); ?>/logout">Logout</a>
                        </div>
                    </div>
                </div>
            </header>
            <div id="maincontainer">
                
                @include('includes.menu')
                

                <div id="container">
                    @yield('maincontent')
                </div>


            </div>
            <footer>
                <p class="copy">&copy; <?php echo $t; ?></p>
                <p class="developer">Developed by: Pramod Bhandari</a></p>
            </footer>
        </div>
    </div>

    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo url('/'); ?>/asset/js/bootstrap.min.js"></script>
    <script src="<?php echo url('/'); ?>/asset/js/confirm.js"></script>
    <script src="<?php echo url('/'); ?>/asset/js/message.js"></script>
    <script src="<?php echo url('/'); ?>/asset/js/menu.js"></script>
</body>
</html>