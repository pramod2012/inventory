@extends('home')

@section('maincontent')

<div id="settings">
    <div class="search_form">
        <h4 class="title_four">Update password</h4>
        <?php 
            if(Session::has('message')){
                echo Session::get('message');
            }
            ?>
        <form action="<?php echo url('/'); ?>/update_password" method="post">
            <label for="cp">Current password</label>
            <input type="password" name="old_pass" id="cp" placeholder="Current password" autocomplete="off">

            <label for="np">New password</label>
            <input type="password" name="new_pass" id="np" placeholder="New password" autocomplete="off">

            <label for="cnp">Confirm new password</label>
            <input type="password" name="con_new_pass" id="cnp" placeholder="Confirm new password" autocomplete="off">
            <div id="pass_status"></div>

            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
            <input type="submit" value="Update" >
        </form>
    </div>
    
</div>
<script>
    $(document).ready(function(){
       $("#cnp").keyup(function(){
          np = $("#np").val();
          np_l = np.length;
          
          cnp = $("#cnp").val();
          cnp_l = cnp.length;
          
          s = $("#pass_status");
          
          if(cnp_l == 0){
              s.html("");
          } else  if(cnp_l > 0 && cnp_l < np_l){
              s.html("matching...");
          } else if(cnp_l == np_l){
              if(cnp === np){
                  s.html("Password matched").css("color", "green");
              } else {
                  s.html("Password not matched").css("color", "red");
              }
          } else if( cnp_l > np_l){
              s.html("Password not matched").css("color", "red");
          }
       });
    });
</script>
@endsection