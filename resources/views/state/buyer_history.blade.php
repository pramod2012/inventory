@extends('home')

@section('maincontent')



@include('scripts.print_history')
<div class="row">
    <h2 class="title_two">Sell History 
        <span><a href="#" onclick="return print_this('report');">Print</a></span>
    </h2>
    <div id="report">
        <div class="stock_table">
            <h3 class="title_three">Buyer Name: <?php echo $title; ?></h3>
            <?php 
            if(isset($from) && !empty($from)){?>
            <div class="date"><?php echo $from . '<span>-</span>' . $to; ?></div>
            <?php } ?>
            <?php if(count($history) > 0) {?>
            <table>
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Image</th>
                        <th>Brand</th>
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Rate</th>
                        <th>Total</th>
                        <th>Paid</th>
                        <th>Due/Receivable</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $total = 0;
                    $total_paid = 0;
                    $due = 0;
                    $receive_due = 0;
                    foreach ($history as $h) {
                        ?>
                        <tr>
                            <td><?php echo $h->sell_date; ?></td>
                            <td>
                                <?php
                                $img = $h->product_image;
                                if (strlen($img) > 3) {
                                    ?>
                                    <img src="<?php echo url('/'); ?>/<?php echo $img; ?>" >
                                <?php } else { ?>
                                    Not Available
    <?php } ?>
                            </td>
                            <td><?php echo $h->brand_name; ?></td>
                            <td><?php echo $h->product_name; ?></td>
                            <td><?php echo $h->sell_quantity; ?></td>
                            <td><?php echo $h->sell_rate; ?></td>
                            <td><?php echo $h->sell_total_price; ?></td>
                            <td>
                                <?php 
                                    $cash_r = $h->sell_received_price; 
                                    $due_r = $h->receive_due;
                                    if($cash_r > 0){
                                        echo $cash_r;
                                    } else {
                                        echo $due_r;
                                    }
                                ?>
                            </td>
                            <td>
                                <?php 
                                    if($h->sell_due_price > 0){
                                        echo $h->sell_due_price;
                                    } 
                                    if($h->receive_due){
                                        echo "- ".$h->receive_due;
                                    }
                                ?>
                            </td>
                            <td>
                                <a id="edit" href="<?php echo url('/'); ?>/"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                <a id="delete" href="<?php echo url('/'); ?>/" onclick="Confirm.check('Confirm !', 'Are you sure to delete this?'); return false;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        <?php
                        $total = $total + $h->sell_total_price;
                        $total_paid = $total_paid + $h->sell_received_price;
                        $due = $due + $h->sell_due_price;
                        $receive_due = $receive_due + $h->receive_due;
                    }
                    ?>
                    <tr>
                        <td colspan="6" >Total:</td>
                        <td colspan="1" ><?php echo $total; ?></td>
                        <td colspan="1" ><?php echo $total_paid + $receive_due; ?></td>
                        <td colspan="1" ><?php echo $due - $receive_due; ?></td>
                        <td><a href="<?php echo url('/'); ?>/receive_due/<?php echo $h->buyer_id; ?>">Receive Due</a></td>
                    </tr>

                </tbody>
            </table>
            <?php } else { ?>
                No data available
            <?php } ?>


        </div>
    </div>
</div>
@include('scripts.history_printstyle')
@endsection

