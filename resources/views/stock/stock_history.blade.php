@extends('home')

@section('maincontent')
@include('style.stock_overwrite')
<div class="row">
    <h2 class="title_two"><?php echo $title; ?></h2>
    
    <div class="stock_table">
        <table>
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Image</th>
                    <th>Brand</th>
                    <th>Name</th>
                    <th>Quantity</th>
                    <th>Rate</th>
                    <th>Total</th>
<!--                    <th>Paid</th>
                    <th>Due/Payable</th>-->
                    <th>Action</th>
                </tr>
            </thead>
            
            <tbody>
                <?php foreach($stocks as $stock) { ?>
                <tr>
                    <td><?php echo $stock->stock_date; ?></td>
                    <td>
                        <?php 
                        $img = $stock->product_image;
                        if(strlen($img) > 3){ ?>
                            <img src="<?php echo url('/'); ?>/<?php echo $img; ?>" >
                        <?php } else {  ?>
                            Not Available
                        <?php } ?>
                    </td>
                    <td><?php echo $stock->brand_name; ?></td>
                    <td><?php echo $stock->product_name; ?></td>
                    <td><?php echo $stock->stock_quantity; ?> (<?php echo $stock->quantity_remains; ?>)</td>
                    <td><?php echo $stock->stock_rate; ?></td>
                    <td><?php echo $stock->total_price; ?></td>
<!--                    <td><?php echo $stock->stock_paid_amount; ?></td>
                    <td><?php echo $stock->stock_due_amount; ?></td>-->
                    <td>
                        <a id="edit" href="<?php echo url('/'); ?>/"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <a id="delete" href="<?php  echo url('/'); ?>/" onclick="Confirm.check('Confirm !', 'Are you sure to delete this?'); return false;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <div class="page_links">
            <?php echo $stocks->links(); ?>
        </div>
        
    </div>
</div>

@endsection

