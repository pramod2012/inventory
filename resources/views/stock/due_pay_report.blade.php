@extends('home')

@section('maincontent')
@include('scripts.printstyle')
<?php

$stock_date = $stock->stock_date;
$supplier_id = $stock->supplier_id;

$due_total = $stock->due_total;
$pay_due = $stock->pay_due;

$supplier = DB::table('tbl_supplier')
        ->where('supplier_id', $supplier_id)
        ->first();
$supplier_name = $supplier->supplier_name;

$site_title = DB::table('tbl_title')
        ->orderBy('title_id', 'desc')
        ->first();
$t = $site_title->title;
?>
@include('scripts.print')
<div id="report">
<div id="stock_report">
    <h3 class="title_three"><?php echo $t; ?></h3>
    <div class="date"><?php echo $stock_date; ?></div>

    <h3 class="title_three supplier"><span>Buyer: </span><?php echo $supplier_name; ?></h3>
    <table>
        <tr>
            <th>Total Due</th><td><?php echo $due_total; ?></td>
        </tr>
        <tr>
            <th>Due Paid</th><td><?php echo $pay_due; ?></td>
        </tr>
        <tr>
            <th>Payable</th><td><?php echo $due_total - $pay_due ?></td>
        </tr>
    </table>
    
    <div class="signature">
        <div class="party_one">Company Sign</div>
        <div class="party_two">Supplier Sign</div>
        <span style="clear: both;"></span>
    </div>
<!--    <div class="footer_link">
        <a href="<?php echo url('/'); ?>">Home</a>
        <a href="<?php echo url(''); ?>/stock">Buyer</a>
    </div>-->
</div>
</div>
    <div id="print_button">
        <a href="#" onclick="return print_this('report');">Print</a>
    </div>

@endsection