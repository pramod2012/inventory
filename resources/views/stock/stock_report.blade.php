@extends('home')

@section('maincontent')
@include('scripts.printstyle')
<?php
$stock_date = $stock->stock_date;
$supplier_id = $stock->supplier_id;
$product_id = $stock->product_id;
$product_quantity = $stock->stock_quantity;
$product_rate = $stock->stock_rate;
$price_total = $stock->total_price;
$paid = $stock->stock_paid_amount;
$due = $stock->stock_due_amount;

$supplier = DB::table('tbl_supplier')
        ->where('supplier_id', $supplier_id)
        ->first();
$supplier_name = $supplier->supplier_name;
$product = DB::table('tbl_product')
        ->where('product_id', $product_id)
        ->first();
$product_name = $product->product_name;

$product_image = $product->product_image;
$site_title = DB::table('tbl_title')
        ->orderBy('title_id', 'desc')
        ->first();
$t = $site_title->title;
?>
@include('scripts.print')
<div id="report">
<div id="stock_report">
    <h3 class="title_three"><?php echo $t; ?></h3>
    <div class="date">1 january 2017</div>

    <h3 class="title_three supplier"><span>Supplier: </span><?php echo $supplier_name; ?></h3>
    <table>
        <tr>
            <th>Product Name</th><td><?php echo $product_name; ?></td>
        </tr>
        <tr>
            <th>Product Quantity</th><td><?php echo $product_quantity; ?></td>
        </tr>
        <tr>
            <th>Product Rate</th><td><?php echo $product_rate; ?></td>
        </tr>
        <tr>
            <th>Total Price</th><td><?php echo $price_total; ?></td>
        </tr>
        <tr>
            <th>Paid</th><td><?php echo $paid; ?></td>
        </tr>
        <tr>
            <th>Due</th><td><?php echo $price_total - $paid; ?></td>
        </tr>
    </table>
    <div class="signature">
        <div class="party_one">Company Sign</div>
        <div class="party_two">Supplier Sign</div>
        <span style="clear: both;"></span>
    </div>
<!--    <div class="footer_link">
        <a href="<?php echo url('/'); ?>">Home</a>
        <a href="<?php echo url(''); ?>/stock">Supplier</a>
    </div>-->
</div>
</div>
    <div id="print_button">
        <a href="#" onclick="return print_this('report');">Print</a>
    </div>

@endsection