@extends('home')

@section('maincontent')
@include('scripts.stock_script')

<h2 class="title_two"><?php echo $title; ?></h2>
<div class="row">

    <div class="basic_form">
        <form action="<?php echo url('/'); ?>/stock" method="post">
            <div id="inventory_head">
<!--                <input id="p" type="radio" name="inventory_type" value="p">
                <label for="p">Purchase</label>

                <input id="s" type="radio" name="inventory_type" value="s">
                <label for="s">Sell</label>-->


                
                <div id="part">
                    <label>Select Category</label>
                    <input type="text" id="category" placeholder="Category Name" autocomplete="off">
                    <input type="hidden" name="category_id" id="category_id" >
                    <div id="category_options">
                        <option value="0">--Select Category--</option>
                        <?php foreach($categories as $c) { ?>
                        <option value="<?php echo $c->category_id; ?>"><?php echo $c->category_name; ?></option>
                        <?php } ?>
                    </div>
                </div>
                
                <div id="part">
                    <label>Select Brand</label>
                    <input type="text" id="brand" placeholder="Brand Name" autocomplete="off">
                    <input type="hidden" name="brand_id" id="brand_id" >
                    <div id="brand_options">
                        <option value="0">--Select Brand--</option>
                        <?php foreach($brands as $b) { ?>
                        <option value="<?php echo $b->brand_id; ?>"><?php echo $b->brand_name; ?></option>
                        <?php } ?>
                    </div>
                </div>
                
                <div id="part">
                    <label>Select Product</label>
                    <input type="text" id="product" placeholder="Product Name" autocomplete="off">
                    <input type="hidden" name="product_id" id="product_id" >
                    <div id="product_options">
                        <option value="0">--Select Product--</option>
                        <?php foreach($products as $p) { ?>
                        <option value="<?php echo $p->product_id; ?>"><?php echo $p->product_name; ?></option>
                        <?php } ?>
                    </div>
                </div>

                <div id="part">
                    <label>Select Supplier</label>
                    <input type="text" id="supplier" placeholder="Supplier Name" autocomplete="off">
                    <input type="hidden" name="supplier_id" id="supplier_id" >
                    <div id="supplier_options">
                        <option value="0">--Select Supplier--</option>
                        <?php foreach($suppliers as $s) { ?>
                        <option value="<?php echo $s->supplier_id; ?>"><?php echo $s->supplier_name; ?></option>
                        <?php } ?>
                    </div>
                </div>
                <div style="clear: both;"></div>
                
            </div>
            <!--  -->
<!--            <div class="form_section">
                <label>Product Name</label>
                <input type="text" name="product_name" placeholder="Product Name">
            </div>-->

            <div class="form_section">
                <label>Product Quantity</label>
                <input type="number" name="stock_quantity" id="quantity" placeholder="Product Quantity">
            </div>
            <div class="form_section">
                <label>Rate</label>
                <input type="number" name="stock_rate" id="rate" placeholder="Each Product Rate">
            </div>
            <div class="form_section">
                <label>Total</label>
                <input type="number" name="total_price" id="total" placeholder="Total Price" disabled="disabled">
            </div>
            <div class="form_section">
                <label>Paid</label>
                <input type="number" name="stock_paid_amount" id="paid" placeholder="Paid">
            </div>
            <div class="form_section">
                <label>Due</label>
                <input type="number" name="stock_due_amount" id="due" placeholder="Due" disabled="disabled">
            </div>
            <div class="form_section">
                <label>Date</label>
                <input type="date" name="stock_date" value="">
            </div>
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
            
            <div class="form_section">
                <input type="submit" value="Add Stock" >
            </div>

            <div style="clear: both;"></div>
        </form>
    </div>

</div>





@endsection
