@extends('home')

@section('maincontent')

<h2 class="title_two"><?php echo $title; ?> from: <?php echo $supplier; ?></h2>
<div class="row">

    <div class="basic_form">
        <?php 
        if(Session::has('message')){
            echo Session::get('message');
        }
        ?>
        <script>
        $(document).ready(function(){
            $("#pay_due").keyup(function(){
               var paid = $(this).val();
               var total = "<?php echo $due_total; ?>";
               var remain = total - paid;
               $("#pay_status").fadeIn("fast");
               $("#due_remain").html(remain);
            });
        });
        </script>
        <form action="<?php echo url('/'); ?>/save_pay" method="post">
            <h5>Total Due: <?php echo $due_total; ?></h5>
            <div class="form_section">
                <label for="pay_due">Due Payd</label>
                <input type="number" name="pay_due" id="pay_due" placeholder="Due Paid">
            </div>
            <div id="pay_status">Due will remain: <span id="due_remain"></span></div>
            <div class="form_section">
                <label>Date</label>
                <input type="date" name="sell_date" value="">
            </div>
            <input type="hidden" name="due_total" value="<?php echo $due_total; ?>" >
            <input type="hidden" name="supplier_id" value="<?php echo $supplier_id; ?>" >
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
            
            <div class="form_section">
                <input type="submit" value="Save" >
            </div>
            
            <div style="clear: both;"></div>
        </form>
    </div>

</div>







@endsection
