@extends('home')

@section('maincontent')
@include('scripts.brand_search')
<h2 class="title_two"><?php echo $title; ?></h2>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div class="simple_form">
            <?php 
            if(Session::has('message')){
                echo Session::get('message');
            }
            ?>
            <form action="<?php echo url('/'); ?>/add_brand" method="post">
                <label for="bn">Brand Name</label>
                <input type="text" name="brand_name" id="bn" placeholder="Brand Name">
                <label for="bd">Brand Description</label>
                <textarea name="brand_description" placeholder="Brand Description"></textarea>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
                <input type="submit" value="Add">
            </form>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div class="search_form">
            <h4 class="title_four">Search Brand</h4>
            <form action="<?php echo url('/'); ?>/brand_history" method="post">
                <label for="brand">Brand</label>
                <input type="text" id="brand" placeholder="Brand Name" autocomplete="off">
                <span id="search_result">
                    
                </span>
                <input type="hidden" name="brand_id" value="" id="brand_id"/>
                <div id="bdata_box">

                    <?php foreach ($all_brands as $p) { ?>
                        <option value="<?php echo $p->brand_id; ?>"><?php echo $p->brand_name; ?></option>
                    <?php } ?>

                </div>
                
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
<!--                <input type="submit" value="Search" >-->
            </form>
        </div>
        <div class="basic_table">
            <table>
                <tr>
                    <th>Brand Name</th>
                    <th>Action</th>
                </tr>
                <?php foreach($brands as $b) { ?>
                <tr>
                    <td>
                        <?php echo $b->brand_name; ?>
                        (<?php 
                        $available = DB::table('tbl_stock')
                                ->where('brand_id', $b->brand_id)
                                ->sum('quantity_remains');
                        echo $available;
                        ?>)
                    </td>
                    <td>
                        <a id="edit" href="<?php echo url('/'); ?>/edit_brand/<?php echo $b->brand_id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <a id="delete" href="<?php  echo url('/'); ?>/delete_brand/<?php echo $b->brand_id; ?>" onclick="Confirm.check('Confirm !', 'Are you sure to delete this?'); return false;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        
                    </td>
                </tr>
                <?php } ?>
            </table>
            <div class="page_links">
                <?php echo $brands->links(); ?>
            </div>
        </div>
    </div>
</div>





@endsection