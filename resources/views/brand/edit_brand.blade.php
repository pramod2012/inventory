@extends('home')

@section('maincontent')
<h2 class="title_two">Edit Brand</h2>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div class="simple_form">
            <?php 
            if(Session::has('message')){
                echo Session::get('message');
            }
            ?>
            <form action="<?php echo url('/'); ?>/update_brand" method="post">
                <label for="pn">Brand Name</label>
                <input type="hidden" name="brand_id" value="<?php echo $brand->brand_id; ?>" >
                <input type="text" name="brand_name" id="pn" value="<?php echo $brand->brand_name; ?>">
                <label for="bd">Brand Description</label>
                <textarea name="brand_description" placeholder="Brand Description"><?php echo $brand->brand_description; ?></textarea>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
                <input type="hidden" name="_method" value="PUT" >
                <input type="submit" value="Update">
            </form>
        </div>
    </div>
    
</div>





@endsection