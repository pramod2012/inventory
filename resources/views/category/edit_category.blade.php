@extends('home')

@section('maincontent')
<h2 class="title_two">Edit Category</h2>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div class="simple_form">
            <?php 
            if(Session::has('message')){
                echo Session::get('message');
            }
            ?>
            <form action="<?php echo url('/'); ?>/update_category" method="post">
                <label for="pn">Category Name</label>
                <input type="hidden" name="category_id" value="<?php echo $category->category_id; ?>" >
                <input type="text" name="category_name" id="pn" value="<?php echo $category->category_name; ?>">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
                <input type="hidden" name="_method" value="PUT" >
                <input type="submit" value="Update">
            </form>
        </div>
    </div>
    
</div>





@endsection