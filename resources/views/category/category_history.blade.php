@extends('home')

@section('maincontent')
@include('style.stock_overwrite')
<div class="row">
    <h2 class="title_two"><?php echo $title; ?></h2>
    
    <div class="stock_table">
        <table>
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Image</th>
                    <th>Brand</th>
                    <th>Name</th>
                    <th>Quantity</th>
                    <th>Rate</th>
                    <th>Total</th>
<!--                    <th>Paid</th>
                    <th>Due/Payable</th>-->
                    <th>Action</th>
                </tr>
            </thead>
            
            <tbody>
                <?php foreach($categories as $category) { ?>
                <tr>
                    <td><?php echo $category->stock_date; ?></td>
                    <td>
                        <?php 
                        $img = $category->product_image;
                        if(strlen($img) > 3){ ?>
                            <img src="<?php echo url('/'); ?>/<?php echo $img; ?>" >
                        <?php } else {  ?>
                            Not Available
                        <?php } ?>
                    </td>
                    <td><?php echo $category->category_name; ?></td>
                    <td><?php echo $category->category_name; ?></td>
                    <td><?php echo $category->stock_quantity; ?> (<?php echo $category->quantity_remains; ?>)</td>
                    <td><?php echo $category->stock_rate; ?></td>
                    <td><?php echo $category->total_price; ?></td>
<!--                    <td><?php echo $category->stock_paid_amount; ?></td>
                    <td><?php echo $category->stock_due_amount; ?></td>-->
                    <td>
                        <a id="edit" href="<?php echo url('/'); ?>/"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <a id="delete" href="<?php  echo url('/'); ?>/" onclick="Confirm.check('Confirm !', 'Are you sure to delete this?'); return false;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <div class="page_links">
            <?php echo $categories->links(); ?>
        </div>
        
    </div>
</div>

@endsection

