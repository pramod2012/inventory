@extends('home')

@section('maincontent')
@include('scripts.category_search')
<h2 class="title_two"><?php echo $title; ?></h2>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div class="simple_form">
            <?php 
            if(Session::has('message')){
                echo Session::get('message');
            }
            ?>
            <form action="<?php echo url('/'); ?>/add_category" method="post">
                <label for="pn">Category Name</label>
                <input type="text" name="category_name" id="pn" placeholder="Category Name">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
                <input type="submit" value="Add">
            </form>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div class="search_form">
            <h4 class="title_four">Search Category</h4>
            <form action="<?php echo url('/'); ?>/category_history" method="post">
                <label for="category">Category</label>
                <input type="text" id="category" placeholder="Category Name" autocomplete="off">
                <span id="search_result">
                    
                </span>
                <input type="hidden" name="category_id" value="" id="category_id"/>
                <div id="bdata_box">

                    <?php foreach ($all_categories as $p) { ?>
                        <option value="<?php echo $p->category_id; ?>"><?php echo $p->category_name; ?></option>
                    <?php } ?>

                </div>
                
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" >
<!--                <input type="submit" value="Search" >-->
            </form>
        </div>
        <div class="basic_table">
            <table>
                <tr>
                    <th>Category Name</th>
                    <th>Action</th>
                </tr>
                <?php foreach($categories as $c) { ?>
                <tr>
                    <td>
                        <?php echo $c->category_name; ?>
                        (<?php 
                            $available = DB::table('tbl_stock')
                                    ->where('category_id', $c->category_id)
                                    ->sum('quantity_remains');
                            echo $available;
                        ?>)
                    </td>
                    <td>
                        <a id="edit" href="<?php echo url('/'); ?>/edit_category/<?php echo $c->category_id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <a id="delete" href="<?php  echo url('/'); ?>/delete_category/<?php echo $c->category_id; ?>" onclick="Confirm.check('Confirm !', 'Are you sure to delete this?'); return false;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        
                    </td>
                </tr>
                <?php } ?>
            </table>
            <div class="page_links">
                <?php echo $categories->links(); ?>
            </div>
        </div>
    </div>
</div>





@endsection